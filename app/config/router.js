import React from 'react';
import { Dimensions } from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation';

import Splash from '../components/splash/splash';
import Login from '../components/authentication/login';
import ListOrder from '../components/orders/listOrder';
import NewOrder from '../components/orders/newOrder';
import OrderDetail from '../components/orders/orderDetail';
import UpdateOrderStatus from '../components/orders/updateOrderStatus';
import OrderHistory from '../components/orders/orderHistory';
import LeftMenu from '../components/menu/leftMenu';
import ShowMap from '../components/maps/showMap';
import Profile from '../components/profile/profile';

const { width, height } = Dimensions.get('window');

export const MainNavigator = StackNavigator({
    ListOrder: {
        screen: ListOrder,
        navigationOptions: {
            header: null
        },
    },
    NewOrder: {
        screen: NewOrder,
        navigationOptions: {
            header: null
        }
    },
    OrderDetail: {
        screen: OrderDetail,
        navigationOptions: {
            header: null
        }
    },
    UpdateOrderStatus: {
        screen: UpdateOrderStatus,
        navigationOptions: {
            header: null
        }
    },
    OrderHistory: {
        screen: OrderHistory,
        navigationOptions: {
            header: null
        }
    },
    ShowMap: {
      screen: ShowMap,
      navigationOptions: {
        header: null
      },
    },
    Profile: {
        screen: Profile,
        navigationOptions: {
            header: null
        },
    },
});

export const SideMenu = DrawerNavigator(
  {
    MainNavigator: {
      screen: MainNavigator
    }
  },
  {
    drawerWidth: width - (width * 0.2),
    drawerPosition: 'left',
    contentComponent: props => <LeftMenu {...props}/>
  }
);

export const Navigator = StackNavigator({

    Splash: { screen : Splash },
    Login: { 
        screen : Login,
        navigationOptions: {
            title: "FirstPage",
            header: null
        }, 
    },
    SideMenu: {
        screen: SideMenu,
        navigationOptions: {
            title: "SideMenu",
            header: null
        }
    }


});