import React, { Component } from 'react';
import MapView from 'react-native-maps';
import { Dimensions, StyleSheet,ActivityIndicator,Text,View, StatusBar, BackHandler,
TouchableOpacity, Platform, Image, Alert, PermissionsAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import * as CONSTANTS from '../../config/constants'
import Communications from 'react-native-communications';
import { connect } from 'react-redux';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 16.003232;
const LONGITUDE = 108.265667;
// const LATITUDE_DELTA = 0.025;
const LATITUDE_DELTA = 0.015;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

const mode = 'driving'; // 'walking';
import flagTo from '../../images/logo_bang.png';
import flagFrom from '../../images/profile.jpg';

// const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${CONSTANTS.GOOGLE_KEY}&mode=${mode}&lang=vi`;
const GEOLOCATION_OPTIONS = { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 };

const defaultProps = {
  enableHack: false,
  geolocationOptions: GEOLOCATION_OPTIONS,
};

class ShowMap extends Component {
    constructor(props) {
        super(props);
        this.mounted = false;
        this.state = {
            marker1: false,
            marker2: false,
            coords: '',
            isLoading: true,
            orderId: '',
            orderCode: '',
            receiverAddress: '',
            receiverDistrict: '',
            receiverPhone: '',
            receiverName: '',
            senderName: '',
            senderAddress: '',
            senderDistrict: '',
            senderLat: '',
            senderLng: '',
            receiverLat: '',
            receiverLng: '',
            myPosition: null,
            calloutIsRendered: false
        };
    }

    decode(t,e) {
        for(var n,o,u=0,l=0,r=0,d= [],h=0,i=0,a=null,c=Math.pow(10,e||5);u<t.length;){a=null,h=0,i=0;do a=t.charCodeAt(u++)-63,i|=(31&a)<<h,h+=5;while(a>=32);n=1&i?~(i>>1):i>>1,h=i=0;do a=t.charCodeAt(u++)-63,i|=(31&a)<<h,h+=5;while(a>=32);o=1&i?~(i>>1):i>>1,l+=n,r+=o,d.push([l/c,r/c])}return d=d.map(function(t){return{latitude:t[0],longitude:t[1]}})
    }

    location(){
        console.log("zo zo zo ");
        
        let navigatorOptions = {}
        if (Platform.OS === 'android' && Platform.Version === 23) {
            navigatorOptions = {
                enableHighAccuracy: true,
                timeout: 10000,
                maximumAge: 0
            }
        }

        navigator.geolocation.getCurrentPosition((position) => {
            console.log("zo zo zo 11");
            var initialPosition = JSON.stringify(position);
            //this.setState({initialPosition});
            console.log("zozo 111" + error.message);
            Alert.alert(position);
        },(error) => {
            console.log("log 111" + error.message);
            Alert.alert("Error 111",error.message);
        },{enableHighAccuracy: true, timeout: 10000}
        );
    }

    watchLocation() {
        // eslint-disable-next-line no-undef
        this.watchID = navigator.geolocation.watchPosition((position) => {
            const myLastPosition = this.state.myPosition;
            var initialPosition = position;
            this.setState({
                senderLat: initialPosition.coords.latitude,
                senderLng: initialPosition.coords.longitude
            }, () => {
                fetch(CONSTANTS.GOOGLE_DIRECTION+this.state.senderLat+","+this.state.senderLng+"&destination="+this.state.receiverLat+","+this.state.receiverLng+"&key="+CONSTANTS.GOOGLE_KEY+"&mode="+mode+"&lang=vi")
                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.routes.length) {
                        this.setState({
                            coords: this.decode(responseJson.routes[0].overview_polyline.points), // definition below
                        });
                    }
                });
            });
        }, (error) => {
            Alert.alert(error.message);
        }, this.props.geolocationOptions);
    }

    componentWillUnmount() {
        this.mounted = false;
        // eslint-disable-next-line no-undef
        if (this.watchID) navigator.geolocation.clearWatch(this.watchID);
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;
        this.mounted = true;

        if (Platform.OS === 'android') {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
            .then(granted => {
                if (granted && this.mounted) this.watchLocation();
            });
        } else {    
            this.watchLocation();
        }

        this.setState({
            orderId: params.orderId,
            orderCode: params.orderCode,
            userToken: params.userToken
        });
        
        //getting order information
        fetch(CONSTANTS.CDV_URL_ORDER_DETAIL+params.orderCode, {
            headers: {
                'Authorization' : params.userToken
            }
        })
        .then((response) => response.json())
        .then((responseJson) => {

            var result = responseJson.result;

            this.setState({
                receiverName: result.receiver.name,
                receiverAddress: result.receiver.address,
                receiverDistrict: result.receiver.district,
                receiverPhone: result.receiver.phone,
                senderName: result.sender.name,
                senderAddress: result.sender.address,
                senderDistrict: result.sender.district,
            }); 

            var origin = result.sender.address+","+result.sender.district+",Đà Nẵng, Việt Nam";
            var destination = result.receiver.address+","+result.receiver.district+",Đà Nẵng, Việt Nam";

            //get lat/long of both address
            //get location of Sender
            fetch(CONSTANTS.GOOGLE_GEO_URL+origin)
            .then((response) => response.json())
            .then((responseJson) => {
                var senderLocation = responseJson.results[0].geometry.location;
                this.setState({
                    senderLat: senderLocation.lat,
                    senderLng: senderLocation.lng
                }, () => {
                    fetch(CONSTANTS.GOOGLE_GEO_URL+destination)
                    .then((response) => response.json())
                    .then((responseJson) => {
                        var receiverLocation = responseJson.results[0].geometry.location;
                        this.setState({
                            receiverLat: receiverLocation.lat,
                            receiverLng: receiverLocation.lng
                        }, () => {
                            fetch(CONSTANTS.GOOGLE_DIRECTION+this.state.senderLat+","+this.state.senderLng+"&destination="+destination+"&key="+CONSTANTS.GOOGLE_KEY+"&mode="+mode+"&lang=vi")
                            .then((response) => response.json())
                            .then((responseJson) => {
                                if (responseJson.routes.length) {
                                    this.setState({
                                        coords: this.decode(responseJson.routes[0].overview_polyline.points), // definition below
                                        isLoading: false
                                    });
                                }
                            });
                        });
                    });
                });
            });
        });
    }

    goBack() {
        this.props.navigation.goBack(null);
    }

    goRefresh() {

    }

    renderCallout() {
        if(this.state.calloutIsRendered === true) return;
        this.setState({calloutIsRendered: true});
        this.marker.showCallout();
    }

    onReceiverPressed() {
        Communications.phonecall(this.state.receiverPhone, true);
    }

    render() {

    var renderData = this.state.isLoading ? (<ActivityIndicator
        style={styles.loading}
        size='large'/>) : (

    <MapView
          style={styles.map}
          region={{
            latitude: this.state.receiverLat,
            longitude: this.state.receiverLng,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}
          onRegionChangeComplete={() => this.renderCallout()}
        >
           <MapView.Marker
            coordinate={{
              latitude: this.state.receiverLat,// + SPACE,
              longitude: this.state.receiverLng,// + SPACE,
            }}
            key={1}
            pinColor={"#16a085"}
            //title={"Marker 1"}
            //description={"Description of marker 1"}
            flat = {false}
            ref={marker => (this.marker = marker)}
            //centerOffset={{ x: -58, y: -80 }}
            //anchor={{ x: 0.4, y: 0.6 }}
          >
            <MapView.Callout onPress={this.onReceiverPressed.bind(this)}>
                <View style={{alignItems: 'center'}}>
                    <Text style={{ padding: 3, fontWeight: 'bold', fontSize: 16}}>{this.state.receiverPhone}</Text>
                    <Text style={{paddingBottom: 3, paddingLeft: 3, paddingRight: 3, fontSize: 14}}>{this.state.senderName} -> {this.state.receiverName}</Text>
                    <Text style={{paddingBottom: 3, paddingLeft: 3, paddingRight: 3, fontSize: 12}}>{this.state.receiverAddress} ,{this.state.receiverDistrict}</Text>
                </View>
            </MapView.Callout>
          </MapView.Marker>
           <MapView.Marker
            coordinate={{
              latitude: this.state.senderLat,// + SPACE,
              longitude: this.state.senderLng,// + SPACE,
            }}
            key={2}
            pinColor={"#c0392b"}
            //centerOffset={{ x: -18, y: -60 }}
            //anchor={{ x: 0.9, y: 1 }}
            
          >
            {/* <Image 
                style={{width: 40, height: 40}}
                source={flagFrom}
            /> */}
          </MapView.Marker>  
          <MapView.Polyline
              coordinates={[
                  {latitude: this.state.senderLat, longitude: this.state.senderLng}, // optional
                  ...this.state.coords,
                  {latitude: this.state.receiverLat, longitude: this.state.receiverLng}, // optional
              ]}
              strokeWidth={7}
              strokeColor={"#3498db"}
          />
        </MapView>      
    )

    return (
        <View style={styles.container}>
            <StatusBar
                barStyle="light-content"
            />
            <View style={styles.topViewContainer}>
            <TouchableOpacity style={styles.topViewButtonLeft} onPress={this.goBack.bind(this)}>
                <Icon name="ios-arrow-back-outline" size={30} color="white" style={styles.iconStyle} />
            </TouchableOpacity>
            <Text style={styles.topViewTitle}>Đơn Hàng #{this.state.orderCode}</Text>
            <TouchableOpacity style={styles.topViewButtonRight} onPress={this.goRefresh.bind(this)}>
                <Icon name="ios-refresh-outline" size={30} color="white" style={styles.iconStyle} />
            </TouchableOpacity>
            </View>
            <View style={{flex: 1}}>
                {renderData}
            </View>
        </View>
    );
  }
}

function mapStateToProps(state) {
    return { myPosition: state.myPosition }
}

export default connect(mapStateToProps)(ShowMap);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    map: {
        flex: 1,
        ...StyleSheet.absoluteFillObject,
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewContainer: {
        height: height / 10,
        backgroundColor: '#1a699e',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    topViewButtonLeft: {
        paddingLeft: 10
    },
    topViewButtonRight: {
        paddingRight: 10
    },
    topViewTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: "700",
        // fontFamily: 'Roboto'
        ...Platform.select({
        android: {
            fontFamily: 'Roboto'
        }
        })
    },
    iconStyle: {
        width: 25,
        height: 25
    },
    itemContentContainer: {
        padding: 10
    },
});