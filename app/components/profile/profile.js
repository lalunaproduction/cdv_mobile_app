import React, { Component } from 'react';
import {
  View, Text, TouchableOpacity, StyleSheet, Image,
  Platform, Dimensions, StatusBar, ScrollView, ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import icMenu from '../../images/ic_menu.png';
import icSearch from '../../images/ic_search.png';
import imgProfile from '../../images/profile.jpg';
import { Fumi } from 'react-native-textinput-effects';
import Button from 'apsl-react-native-button';
import * as CONSTANTS from '../../config/constants';

const drawerIcon = <Icon name="navicon" size={50} color="#000" />

const { width, height } = Dimensions.get('window');

export default class Profile extends Component {

  constructor(props) {
    super(props);

    this.state = {
        userId: '',
        userName: '',
        userAddress: '',
        userDistrict: '',
        userCity: '',
        userPhone: '',
        userEmail: '',
        isLoading: true
        }
    }

    openDrawer() {
        this.props.navigation.navigate('DrawerOpen');
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;

        fetch(CONSTANTS.CDV_URL_GET_PROFILE, {
            headers: {
                'Authorization': params.userToken
            }
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if (responseJson.status == "OK") {
                var result = responseJson.result;
                this.setState({
                    userName: result.profile.full_name,
                    userAddress: result.profile.address,
                    userDistrict: result.profile.district,
                    userPhone: result.phone,
                    userEmail: result.email,
                    isLoading: false
                });
            }
        });

    }

    render() {
        var contentData = this.state.isLoading ? (
        <ActivityIndicator
            style={styles.loading}
            size='large'/>
        ) : (
        <ScrollView style={styles.dataContainer}>
            <View style={styles.profileContainer}>
                <View style={styles.userProfileContainer}>
                    <Image style={styles.userProfile} source={imgProfile} />
                    <Text style={styles.welcome}>{this.state.userName}</Text>
                </View>
            </View>
            <View style={styles.dashboardContainer}>
                <View style={styles.profileCompletedContainer}>
                    <Text style={styles.bigNumber}>10%</Text>
                    <Text style={styles.dashboardText}>Thông Tin</Text>
                </View>
                <View style={styles.profileCompletedContainer}>
                    <Text style={styles.bigNumber}>25</Text>
                    <Text style={styles.dashboardText}>Đơn Hàng</Text>
                </View>
                <View style={styles.profileCompletedContainer}>
                    <Text style={styles.bigNumber}>22</Text>
                    <Text style={styles.dashboardText}>Thành Công</Text>
                </View>
            </View>
            <View style={styles.infoContainer}>
                <Text style={styles.senderInformation}>THÔNG TIN CÁ NHÂN</Text>
                <View style={styles.senderNameInfomation}>
                    <Fumi
                        label={'Tên Đầy Đủ'}
                        ref= {(el) => { this.userName = el; }}
                        onChangeText={(userName) => this.setState({userName})}
                        value={this.state.userName}
                        iconClass={Icon}
                        iconName={'ios-contact-outline'}
                        iconColor={'#16a085'}
                        height={50}
                    />
                </View>
                <View style={styles.senderAddressInfomation}>
                    <Fumi
                        label={'Địa Chỉ (Số Nhà, Tên Đường)'}
                        ref= {(el) => { this.userAddress = el; }}
                        onChangeText={(userAddress) => this.setState({userAddress})}
                        value={this.state.userAddress}
                        iconClass={Icon}
                        iconName={'ios-pin-outline'}
                        iconColor={'#16a085'}
                        height={50}
                    />
                </View>
                <View style={styles.senderAddressInfomation}>
                    <Fumi
                        label={'Số Điện Thoại'}
                        ref= {(el) => { this.userPhone = el; }}
                        onChangeText={(userPhone) => this.setState({userPhone})}
                        value={this.state.userPhone}
                        iconClass={Icon}
                        iconName={'ios-call-outline'}
                        iconColor={'#16a085'}
                        height={50}
                    />
                </View>
                <View style={styles.senderAddressInfomation}>
                    <Fumi
                        label={'Địa chỉ Email'}
                        ref= {(el) => { this.userEmail = el; }}
                        onChangeText={(userEmail) => this.setState({userEmail})}
                        value={this.state.userEmail}
                        iconClass={Icon}
                        iconName={'ios-mail-outline'}
                        iconColor={'#16a085'}
                        height={50}
                    />
                </View>
                <View style={styles.senderAddressInfomation}>
                    <Button
                        style={styles.buttonCaculateStyle}
                        isLoading={false}
                        textStyle={styles.buttonTextStyle}
                        onPress={() => {
                            console.log('world!')
                        }}>
                        CẬP NHẬT
                    </Button>
                </View>
            </View>
        </ScrollView>
        )

        return (
        <View style={styles.container}>
            <StatusBar
            hidden={true}
            />
            <View style={styles.topViewContainer}>
            <TouchableOpacity style={styles.topViewButtonLeft} onPress={this.openDrawer.bind(this)}>
                <Image style={styles.iconStyle} source={icMenu} />
            </TouchableOpacity>
            <Image style={{height: 40, width: 55}} source={require('../../images/cdv.png')}/>
            <TouchableOpacity style={styles.topViewButtonRight}>
                <Image style={styles.iconStyle} source={icSearch} />
            </TouchableOpacity>
            </View>
            { contentData }
        </View>
        )
    }
    }

    const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    topViewContainer: {
        height: height / 10,
        backgroundColor: '#1a699e',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    topViewButtonLeft: {
        paddingLeft: 10
    },
    topViewButtonRight: {
        paddingRight: 10
    },
    topViewTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: "700",
        // fontFamily: 'Roboto'
        ...Platform.select({
        android: {
            fontFamily: 'Roboto'
        }
        })
    },
    iconStyle: {
        width: 25,
        height: 25
    },
    mainItemsContainer: {
        // backgroundColor: '#fff',
        margin: 10,
        shadowColor: '#4e4e4f',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2
    },
    profileContainer: {
        height: height / 5,
        backgroundColor: '#206ea3'
    },
    userProfileContainer: {
        alignItems: 'center',
        paddingTop: 10
    },
    userProfile: {
        width: 80,
        height: 80,
        borderRadius: 40
    },
    welcome: {
        paddingTop: 10,
        color: 'white',
        fontWeight: '800',
        fontSize: 15
    },
    dashboardContainer: {
        height: 70,
        flexDirection: 'row'
    },
    profileCompletedContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    numberOfOrderContainer: {
        flex: 1,
        backgroundColor: 'yellow',
        alignItems: 'center'
    },
    completedOrderContainer: {
        flex: 1,
        backgroundColor: 'green',
        alignItems: 'center'
    },
    bigNumber: {
        fontSize: 22,
        fontWeight: '800',
        color: '#3498db'
    },
    dashboardText: {
        paddingTop: 5,
        fontSize: 13,
        fontWeight: '600',
    },
    infoContainer: {
        backgroundColor: '#F5F5F5',
        shadowColor: '#4e4e4f',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        marginBottom: 10
    },
    senderInformation: {
        paddingLeft: 10,
        paddingTop: 10,
        paddingRight: 10,
        fontSize: 13,
        color: '#2980b9',
        fontWeight: '900'
    },
    senderNameInfomation: {
        padding: 10
    },
    senderAddressInfomation: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonCaculateStyle: {
        borderColor: '#2980b9',
        backgroundColor: '#2980b9'
    },
    buttonTextStyle: {
        color: 'white'
    }
    });
