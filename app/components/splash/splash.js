import React, { Component } from 'react';
import { View, Text, StyleSheet,Image, TouchableOpacity, 
    AsyncStorage,StatusBar } from 'react-native';
import { NavigationActions } from 'react-navigation';

//import SideMenu from '../../config/router';

export default class Splash extends Component {

  static navigationOptions = {
    header: null
  };

//   checkLoggedIn = async()=>{
//     try {
//       var data = await AsyncStorage.getItem("USERNAME");
//       return data;
//     } catch(e) {
//       console.log(e);
//     }
//   }

  componentDidMount() {
    const { navigate } = this.props.navigation;
    
    AsyncStorage.getItem("USERTOKEN").then((value)=>{
        if(value == null) {
            setTimeout(() => {
            navigate('Login')
            }, 2000);
        } 
        else {
            setTimeout(() => {
                this.props.navigation.dispatch(
                  NavigationActions.reset({
                    index: 0,
                    actions: [
                      NavigationActions.navigate({ 
                        routeName: 'SideMenu', 
                        params: {userToken: value}
                      })
                    ]
                  }));
            }, 2000);
        }
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <StatusBar
          hidden={true}
        />
        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require('../../images/cdvExpress.png')}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#2980b9"
  },
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  logo: {
    width: 270,
    height: 220
  },
  title: {
    color: '#FFF',
    fontSize: 18,
    fontWeight: '800',
    width: 220,
    textAlign: 'center',
    opacity: 0.9
  }
});