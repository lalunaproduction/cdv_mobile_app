import React, { Component } from 'react';
import {
    View, Text, StyleSheet,
    TouchableOpacity, Alert,
    Platform, Image, AsyncStorage
    } from 'react-native';

    import Icon from 'react-native-vector-icons/FontAwesome';
    import imgProfile from '../../images/profile.jpg';

    export default class LeftMenu extends Component {

    constructor(props) {
        super(props);

        this.state = {
            userId: '',
            userPhone: '',
            userRole: '',
            userToken: '',
        }
    }

    onProfileClicked() {
        const { navigate } = this.props.navigation;
        navigate('Profile', {
            userToken : this.state.userToken
        });
    }

    onMyOrderClicked() {
        const { navigate } = this.props.navigation;
        navigate('SideMenu', {
            userId : this.state.userId
        });
    }

    logout = async()=>{
        AsyncStorage.removeItem("USERID").then(()=>{
            AsyncStorage.removeItem("USERPHONE").then(()=>{
                AsyncStorage.removeItem("USERROLE").then(() => {
                    AsyncStorage.removeItem("USERTOKEN").then(() => {
                        const { navigate } = this.props.navigation;
                        navigate('Login');
                    });
                });
            });
        });
    }

    componentDidMount() {
        AsyncStorage.getItem("USERID").then((response) => {
            this.setState({
                userId: response
            });
        });

        AsyncStorage.getItem("USERPHONE").then((response) => {
            this.setState({
                userPhone: response
            });
        });

        AsyncStorage.getItem("USERROLE").then((response) => {
            this.setState({
                userRole: response
            });
        });

        AsyncStorage.getItem("USERTOKEN").then((response) => {
            this.setState({
                userToken: response
            })
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.userProfileContainer}>
                <Image style={styles.userProfile} source={imgProfile} />
                <Text style={styles.welcome}>XIN CHÀO, {this.state.userPhone}</Text>
                </View>
                <TouchableOpacity style={styles.drawerItemContainer} onPress={this.onProfileClicked.bind(this)}>
                <View style={styles.drawerItem}>
                    <Icon name="user-o" size={20} color="#fff" />
                    <Text style={styles.drawerItemText}>THÔNG TIN CÁ NHÂN</Text>
                </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerItemContainer} onPress={this.onMyOrderClicked.bind(this)}>
                <View style={styles.drawerItem}>
                    <Icon name="user-o" size={20} color="#fff" />
                    <Text style={styles.drawerItemText}>ĐƠN HÀNG CỦA TÔI</Text>
                </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerItemContainer} onPress={this.logout.bind(this)}>
                <View style={styles.drawerItem}>
                    <Icon name="power-off" size={20} color="#fff" />
                    <Text style={styles.drawerItemText}>ĐĂNG XUẤT</Text>
                </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0e4970'
    },
    userProfileContainer: {
        alignItems: 'center',
        paddingTop: 15
    },
    userProfile: {
        width: 120,
        height: 120,
        borderRadius: 60
    },
    welcome: {
        paddingTop: 5,
        color: '#fff',
        // fontFamily: 'Roboto',
        ...Platform.select({
        android: {
            fontFamily: 'Roboto'
        }
        }),
        fontWeight: "600"
    },
    drawerItemContainer: {
        paddingTop: 30
    },
    drawerItem: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10
    },
    drawerItemText: {
        color: '#fff',
        paddingLeft: 15,
        // fontFamily: 'Roboto',
        ...Platform.select({
        android: {
            fontFamily: 'Roboto'
        }
        }),
        fontSize: 15,
        fontWeight: "600"
    }
});
