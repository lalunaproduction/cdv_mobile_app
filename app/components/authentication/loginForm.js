import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, ActivityIndicator, AsyncStorage, Alert, Keyboard } from 'react-native';
import { Text, View } from 'react-native-animatable';
import { NavigationActions } from 'react-navigation';

import CustomButton from '../../components/customs/CustomButton';
import CustomTextInput from '../../components/customs/CustomTextInput';
import * as CONSTANTS from '../../config/constants';
import metrics from '../../config/metrics';

export default class LoginForm extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            phone: '',
            password: '',
            fullName: '',
            isLoading: false
        }
    }

    onLoginPress = async()=>{

        Keyboard.dismiss();

        this.setState({
            isLoading: true
        });

        var prmPhone    = this.state.phone;
        var prmPassword = this.state.password;

        fetch(CONSTANTS.CDV_URL_LOGIN, {
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: prmPhone,
                password: prmPassword
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
            //login thanh cong
            console.log("ok eko");
            if (responseJson.code == 200) {
                var usr = responseJson.result;
                try {
                    console.log("Login thanh cong");
                    //Lưu thông tin userID vào asyncStorage
                    AsyncStorage.setItem("USERID", ''+usr.id).then(() => {
                        //Lưu thông tin UserPhone vào asyncStorage
                        AsyncStorage.setItem("USERPHONE", usr.phone).then(() => {
                            //Lưu Role của người dùng vào asyncStorage
                            AsyncStorage.setItem("USERROLE", usr.role.name).then(() => {
                                //Lưu Token của người dùng vào asyncStorage
                                AsyncStorage.setItem("USERTOKEN", usr.token).then(() => {
                                    this.setState({
                                        isLoading: false
                                    });
                                    //Di chuyển đến màn hình list các order sau khi lưu xong
                                    // this.props.navigate('MainNavigator', {
                                    //     //userToken: usr.token
                                    // });
                                    this.props.navigate('MainNavigator', {}, {
                                        type: "Navigation/NAVIGATE",
                                        routeName: "ListOrder",
                                        params: {
                                            phone: usr.phone,
                                            id: usr.id,
                                            role: usr.role.name,
                                            userToken: usr.token
                                        }
                                    });
                                });
                            });
                        });
                    });
                } catch (e) {
                    console.log(e);
                }
            } else {
                this.setState({
                    isLoading: false
                });

                Alert.alert(
                    'Đăng Nhập Thất Bại',
                    'Vui lòng kiểm tra lại SĐT/Email hoặc Mật Khẩu của bạn.',
                    [
                        {text: 'OK', onPress: () => console.log("click ok")},
                    ]
                )
            }
        });
    }

    hideForm = async () => {
        if (this.buttonRef && this.formRef && this.linkRef) {
            await Promise.all([
                this.buttonRef.zoomOut(200),
                this.formRef.fadeOut(300),
                this.linkRef.fadeOut(300)
            ])
        }
    }

    handleKeyDown(e) {
        if(e.nativeEvent.key == "Enter"){
            Keyboard.dismiss();
        }
    }

    render () {
        const { email, password } = this.state
        const { isLoading, onSignupLinkPress, onLoginPress } = this.props
        const isValid = email !== '' && password !== ''
        return (
            <View style={styles.container}>
                { this.state.isLoading == true ? <ActivityIndicator style={styles.loading} size='large'/> : null}
                <View style={styles.form} ref={(ref) => { this.formRef = ref }}>
                    <CustomTextInput
                        name={'phone'}
                        ref={(ref) => this.emailInputRef = ref}
                        placeholder={'Số điện thoại hoặc Email'}
                        keyboardType={'email-address'}
                        editable={!isLoading}
                        returnKeyType={'next'}
                        blurOnSubmit={false}
                        withRef={true}
                        onSubmitEditing={() => this.passwordInputRef.focus()}
                        onChangeText={(value) => this.setState({ phone: value })}
                        isEnabled={!isLoading}
                    />
                    <CustomTextInput
                        name={'password'}
                        ref={(ref) => this.passwordInputRef = ref}
                        placeholder={'Mật Khẩu'}
                        editable={!isLoading}
                        returnKeyType={'done'}
                        secureTextEntry={true}
                        onKeyPress={this.handleKeyDown.bind(this)}
                        withRef={true}
                        onChangeText={(value) => this.setState({ password: value })}
                        isEnabled={!isLoading}
                    />
                </View>
                <View style={styles.footer}>
                    <View ref={(ref) => this.buttonRef = ref} animation={'bounceIn'} duration={600} delay={400}>
                        <CustomButton
                            onPress={this.onLoginPress.bind(this)}
                            isEnabled={isValid}
                            isLoading={isLoading}
                            buttonStyle={styles.loginButton}
                            textStyle={styles.loginButtonText}
                            text={'Đăng Nhập'}
                        />
                    </View>
                    <Text
                        ref={(ref) => this.linkRef = ref}
                        style={styles.signupLink}
                        onPress={onSignupLinkPress}
                        animation={'fadeIn'}
                        duration={600}
                        delay={400}
                    >
                        {'Chưa có tài khoản?'}
                    </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: metrics.DEVICE_WIDTH * 0.1,
        backgroundColor: '#0e3650'
    },
    form: {
        marginTop: 20
    },
    footer: {
        height: 100,
        justifyContent: 'center'
    },
    loginButton: {
        backgroundColor: 'white'
    },
    loginButtonText: {
        color: '#00796B',
        fontWeight: 'bold'
    },
    signupLink: {
        color: 'rgba(255,255,255,0.6)',
        alignSelf: 'center',
        padding: 20
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})