import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Keyboard, ActivityIndicator, Alert } from 'react-native';
import { Text, View } from 'react-native-animatable';

import CustomButton from '../../components/customs/CustomButton';
import CustomTextInput from '../../components/customs/CustomTextInput';
import metrics from '../../config/metrics';

export default class SignupForm extends Component { 
    constructor(props) {
        super(props);

        this.state = {
            phone: '',
            password: '',
            email: '',
            isLoading: false
        }
    }

    hideForm = async () => {
        if (this.buttonRef && this.formRef && this.linkRef) {
            await Promise.all([
                this.buttonRef.zoomOut(200),
                this.formRef.fadeOut(300),
                this.linkRef.fadeOut(300)
            ])
        }
    }

    onSignupPress = async() => {
        Keyboard.dismiss();

        // this.setState({ isLoading: true });

        var prmPhone    = this.state.phone;
        var prmPassword = this.state.password;
        var prmEmail    = this.state.email;

        Alert.alert("CDV Thông Báo", "Ứng dụng CDV Express hiện tại chỉ đang cung cấp cho đối tác của CDV. Xin vui lòng liên hệ để biết thêm chi tiết",
        [
           { text: 'OK' }
        ]);
    }

    render() {
        const { email, password, fullName } = this.state;
        const { isLoading, onLoginLinkPress, onSignupPress } = this.props;
        const isValid = email !== '' && password !== '' && fullName !== '';

        return (
            <View style={styles.container}>
                { this.state.isLoading == true ? <ActivityIndicator style={styles.loading} size='large'/> : null}
                <View style={styles.form} ref={(ref) => this.formRef = ref}>
                    <CustomTextInput
                        name={'phone'}
                        ref={(ref) => this.mobileInputRef = ref}
                        placeholder={'Số Điện Thoại'}
                        editable={!isLoading}
                        returnKeyType={'next'}
                        blurOnSubmit={false}
                        withRef={true}
                        onSubmitEditing={() => this.emailInputRef.focus()}
                        onChangeText={(value) => this.setState({ phone: value })}
                        isEnabled={!isLoading}
                    />
                    <CustomTextInput
                        ref={(ref) => this.emailInputRef = ref}
                        placeholder={'Email'}
                        keyboardType={'email-address'}
                        editable={!isLoading}
                        returnKeyType={'next'}
                        blurOnSubmit={false}
                        withRef={true}
                        onSubmitEditing={() => this.passwordInputRef.focus()}
                        onChangeText={(value) => this.setState({ email: value })}
                        isEnabled={!isLoading}
                    />
                    <CustomTextInput
                        ref={(ref) => this.passwordInputRef = ref}
                        placeholder={'Mật Khẩu'}
                        editable={!isLoading}
                        returnKeyType={'done'}
                        secureTextEntry={true}
                        withRef={true}
                        onChangeText={(value) => this.setState({ password: value })}
                        isEnabled={!isLoading}
                    />
                </View>
                <View style={styles.footer}>
                    <View ref={(ref) => this.buttonRef = ref} animation={'bounceIn'} duration={600} delay={400}>
                        <CustomButton
                        onPress={this.onSignupPress.bind()}
                        isEnabled={isValid}
                        isLoading={isLoading}
                        buttonStyle={styles.createAccountButton}
                        textStyle={styles.createAccountButtonText}
                        text={'Đăng Ký'}
                        />
                    </View>
                    <Text
                        ref={(ref) => this.linkRef = ref}
                        style={styles.loginLink}
                        onPress={onLoginLinkPress}
                        animation={'fadeIn'}
                        duration={600}
                        delay={400}
                    >
                        {'Tôi đã có Tài Khoản'}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: metrics.DEVICE_WIDTH * 0.1,
    backgroundColor: '#0e3650'
  },
  form: {
    marginTop: 20
  },
  footer: {
    height: 100,
    justifyContent: 'center'
  },
  createAccountButton: {
    backgroundColor: 'white'
  },
  createAccountButtonText: {
    color: '#3E464D',
    fontWeight: 'bold'
  },
  loginLink: {
    color: 'rgba(255,255,255,0.6)',
    alignSelf: 'center',
    padding: 20
  }
})