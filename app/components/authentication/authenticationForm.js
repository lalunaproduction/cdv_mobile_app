import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { Text, View } from 'react-native-animatable';

import CustomButton from '../../components/customs/CustomButton';
import metrics from '../../config/metrics';

export default class AuthenticationForm extends Component { 
    static propTypes = {
        onCreateAccountPress: PropTypes.func.isRequired,
        onSignInPress: PropTypes.func.isRequired
    }

    render () {
        return (
            <View style={styles.container}>
                <View animation={'zoomIn'} delay={600} duration={400}>
                    <CustomButton
                        text={'Đăng Ký'}
                        onPress={this.props.onCreateAccountPress}
                        buttonStyle={styles.createAccountButton}
                        textStyle={styles.createAccountButtonText}
                    />
                </View>
                <View style={styles.separatorContainer} animation={'zoomIn'} delay={700} duration={400}>
                    <View style={styles.separatorLine} />
                    <Text style={styles.separatorOr}>{'hoặc'}</Text>
                    <View style={styles.separatorLine} />
                </View>
                <View animation={'zoomIn'} delay={800} duration={400}>
                    <CustomButton
                        text={'Đăng Nhập'}
                        onPress={this.props.onSignInPress}
                        buttonStyle={styles.signInButton}
                        textStyle={styles.signInButtonText}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: metrics.DEVICE_WIDTH * 0.1,
        justifyContent: 'center'
    },
    createAccountButton: {
        borderColor: 'white',
        backgroundColor: 'white'
    },
    createAccountButtonText: {
        color: '#16a085'
    },
    separatorContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        marginVertical: 20
    },
    separatorLine: {
        flex: 1,
        borderWidth: StyleSheet.hairlineWidth,
        height: StyleSheet.hairlineWidth,
        borderColor: '#013d65'
    },
    separatorOr: {
        color: '#013d65',
        marginHorizontal: 8
    },
    signInButton: {
        backgroundColor: '#0e3650'
    },
    signInButtonText: {
        color: 'white',
        fontWeight: '700'
    }
});