import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput,
Dimensions, Platform, StatusBar,Keyboard, BackHandler,
ListView, ScrollView, Alert, ActivityIndicator } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import metrics from '../../config/metrics'
import * as CONSTANTS from '../../config/constants';
import Timeline from 'react-native-timeline-listview'

const { width, height } = Dimensions.get('window');
const primaryStep = "#2c3e50";
const successStep = "#16a085";
const errorStep = "#c0392b";
const startStep = "#8e44ad";

export default class OrderHistory extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            historyData: [],
            isLoading: true
        }
    }

    goBack() {
        this.props.navigation.goBack(null);
    }

    goRefresh() {
    }

    componentWillMount() {
        var that = this;
        BackHandler.addEventListener('hardwareBackPress', function() {
            that.goBack.bind(this);
        });
    }

    componentDidMount() {
        
        const { params } = this.props.navigation.state;
        this.setState({
            orderCode: params.orderCode
        });

        fetch(CONSTANTS.CDV_URL_GET_ORDER_HISTORY+params.orderCode, {
            headers: {
                'Authorization' : params.userToken
            }
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.status === "OK") {
                this.setState({
                    historyData: responseJson.result,
                    isLoading: false
                })
            }
        });
    }

    render() {
        var renderData = this.state.isLoading ? 
        (<ActivityIndicator
            style={styles.loading}
            size='large'/>) : 
        (
            <View style={styles.timeLine}>
                <Timeline 
                    style={styles.list}
                    data={this.state.historyData}
                    circleSize={20}
                    circleColor='rgb(45,156,219)'
                    timeContainerStyle={{minWidth:52, marginTop: 0}}
                    timeStyle={{textAlign: 'center', backgroundColor:'#2980b9', color:'white', padding:10, borderRadius:13}}
                    descriptionStyle={{color:'gray'}}
                    options={{
                        style:{paddingTop:5},
                        enableEmptySections:true
                    }}
                    enableEmptySections={true}
                />
            </View>
        )
        return (

            <View style={styles.container}>
                <StatusBar
                    barStyle="light-content"
                />
                <View style={styles.topViewContainer}>
                    <TouchableOpacity style={styles.topViewButtonLeft} onPress={this.goBack.bind(this)}>
                        <Icon name="ios-arrow-back-outline" size={30} color="white" style={styles.iconStyle} />
                    </TouchableOpacity>
                    <Text style={styles.topViewTitle}>Lịch Sử Đơn Hàng #{this.state.orderCode}</Text>
                    <TouchableOpacity style={styles.topViewButtonRight} onPress={this.goRefresh.bind(this)}>
                        <Icon name="ios-refresh-outline" size={30} color="white" style={styles.iconStyle} />
                    </TouchableOpacity>
                </View>
                {renderData}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA'
    },
    orderStatusContainer: {
        padding: 10,
        flexDirection: 'row',
    },
    shipperContainer: {
        padding: 10,
        flexDirection: 'row'
    },
    contentContainer: {
        padding: 10
    },
    topViewContainer: {
        height: height / 10,
        backgroundColor: '#1a699e',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    timeLine: {
        flex: 1,
    },
    list: {
        flex: 1,
        margin:5,
    },
    topViewButtonLeft: {
        paddingLeft: 10
    },
    topViewButtonRight: {
        paddingRight: 10
    },
    topViewTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: "700",
        // fontFamily: 'Roboto'
        ...Platform.select({
        android: {
            fontFamily: 'Roboto'
        }
        })
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
});