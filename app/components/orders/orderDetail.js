import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput,
Dimensions, Platform, Image, PropTypes, StatusBar, BackHandler,
ListView, ActivityIndicator, AsyncStorage, ScrollView, Alert } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';
import icMenu from '../../images/ic_menu.png';
import icSearch from '../../images/ic_search.png';
import metrics from '../../config/metrics'
import Geocoder from 'react-native-geocoder';
import { Fumi } from 'react-native-textinput-effects';
import * as CONSTANTS from '../../config/constants';
import StepIndicator from 'react-native-step-indicator';
import Communications from 'react-native-communications';
import PopupDialog, { DialogTitle, SlideAnimation, DialogButton } from 'react-native-popup-dialog';
import Picker from 'react-native-picker';

const drawerIcon = <Icon name="navicon" size={50} color="#000" />
const { width, height } = Dimensions.get('window');
const upIcon = <Icon name="ios-arrow-up-outline" size={30} color="#fff" />
const refreshIcon = <Icon name="ios-refresh-outline" size={30} color="#fff" />

const thirdIndicatorStyles = {
    stepIndicatorSize: 20,
    currentStepIndicatorSize:30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#7eaec4',
    stepStrokeWidth: 2,
    stepStrokeFinishedColor: '#2980b9',
    stepStrokeUnFinishedColor: '#dedede',
    separatorFinishedColor: '#2980b9',
    separatorUnFinishedColor: '#dedede',
    stepIndicatorFinishedColor: '#2980b9',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 0,
    currentStepIndicatorLabelFontSize: 0,
    stepIndicatorLabelCurrentColor: 'transparent',
    stepIndicatorLabelFinishedColor: 'transparent',
    stepIndicatorLabelUnFinishedColor: 'transparent',
    labelColor: '#2980b9',
    labelSize: 13,
    currentStepLabelColor: '#7eaec4'
};

export default class OrderDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            orderId: '',
            orderCode: '',
            orderStatus: '',
            orderStatusId: '',
            orderType: '',
            senderName: '',
            senderAddress: '',
            senderPhone: '',
            receiverName: '',
            receiverAddress: '',
            receiverPhone: '',
            promotionCode: '',
            totalPrice: '',
            currentPosition: 0,
            distance: '',
            currentUserId: '',
            userRole: '',
            userToken: '',
            shipperId: '',
            shipperName: '',
            shipperPhone: '',
            shipperToken: '',
            newOrderStatus: '',
            newOrderStatusId: 0,
            listShipper: [],
            shipperFullName: '',
            shipperId: 0,
            cod: 0,
            payer: '',
            packageNote: ''
        }
    }

    updateOrderData(orderCode) {
        this.setState({
            orderCode: orderCode
        }, () => {
            this.goRefresh();
        })
    }

    componentWillMount() {
        AsyncStorage.getItem("USERROLE").then((value)=>{
            this.setState({
                userRole: value
            });
        });
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;
        this.setState({ orderCode: params.code, userToken: params.userToken, isLoading: true });
        
        fetch(CONSTANTS.CDV_URL_ORDER_DETAIL+params.code, {
            headers: {
                'Authorization': params.userToken 
            }
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log("zo oz zo");
            console.log(responseJson);
            var result = responseJson.result;
            console.log(result);

            var currentPos = 2;
            if (result.order_status.id < 4) {
                currentPos = 1;
            } else if (result.order_status.id == 7 || result.order_status.id == 11) {
                currentPos = 3;
            }

            this.setState({
                orderStatus: result.order_status.name,
                orderStatusId: result.order_status.id,
                orderId: result.id,
                orderCode: result.order_code,
                orderType: result.ship_type.name,
                senderName: result.sender.name,
                senderAddress: result.sender.address + ' - ' + result.sender.district + ' - ' + result.sender.city,
                senderPhone: result.sender.phone,
                receiverName: result.receiver.name,
                receiverAddress: result.receiver.address + ' - ' + result.receiver.district + ' - ' + result.receiver.city,
                receiverPhone: result.receiver.phone,
                promotionCode: result.promotion_id,
                totalPrice: result.total,
                currentPosition: currentPos,
                shipperId: result.shipper_id,
                newOrderStatus: result.order_status.name,
                cod: result.cod,
                payer: result.payer,
                packageNote: result.package.note
            });
            
            var origin = result.sender.address+","+result.sender.district+",Đà Nẵng,Việt Nam";
            var destination = result.receiver.address+","+result.receiver.district+",Đà Nẵng,Việt Nam";
            
            console.log(origin);
            console.log(destination);

            //get shipper infor if any
            if (result.shipper_id != 0) {
                fetch(CONSTANTS.CDV_URL_SHIPPER_INFO+result.shipper_id, {
                    headers: {
                        'Authorization': params.userToken
                    }
                })
                .then((response) => response.json())
                .then((responseJson) => {

                    this.setState({
                        shipperName: responseJson.result.profile.full_name,
                        shipperPhone: responseJson.result.phone,
                        shipperToken: responseJson.result.token
                    });
                    //get distance value
                    fetch(CONSTANTS.GOOGLE_URL+'origin='+origin+'&destination='+destination+'&key='+CONSTANTS.GOOGLE_KEY+'&mode=json&language=vi')
                    .then((response) => response.json())
                    .then((responseJson) => {

                        this.setState({
                            distance: responseJson.routes[0].legs[0].distance.text,
                            isLoading: false
                        });
                    });
                })
            } else {
                console.log(CONSTANTS.GOOGLE_URL+'origin='+origin+'&destination='+destination+'&key='+CONSTANTS.GOOGLE_KEY+'&mode=json&language=vi');
                fetch(CONSTANTS.GOOGLE_URL+'origin='+origin+'&destination='+destination+'&key='+CONSTANTS.GOOGLE_KEY+'&mode=json&language=vi')
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log("bla bla");
                    console.log(responseJson);
                    this.setState({
                        distance: responseJson.routes[0].legs[0].distance.text,
                        isLoading: false
                    });
                });
            }
        })
        .catch(function(error) {
            Alert.alert(
                'Network Request Failed',
                'Có Lỗi về vấn đề đường truyền. Vui lòng kiểm tra lại kết nối Internet',
                [
                    { text: 'OK' }
                ]
            );
        });

        var that = this;

        BackHandler.addEventListener('hardwareBackPress', function() {
            that.goBack.bind(this);
        });

    }

    goBack() {
        this.props.navigation.goBack(null);
    }

    goRefresh() {

        this.setState({
            isLoading: true
        });

        fetch(CONSTANTS.CDV_URL_ORDER_DETAIL+this.state.orderCode, {
            headers: {
                'Authorization': this.state.userToken 
            }
        })
        .then((response) => response.json())
        .then((responseJson) => {
            var result = responseJson.result;
            var currentPos = 2;
            if (result.order_status.id < 4) {
                currentPos = 1;
            } else if (result.order_status.id == 7 || result.order_status.id == 11) {
                currentPos = 3;
            }

            this.setState({
                orderStatus: result.order_status.name,
                orderStatusId: result.order_status.id,
                orderId: result.id,
                orderCode: result.order_code,
                orderType: result.ship_type.name,
                senderName: result.sender.name,
                senderAddress: result.sender.address + ' - ' + result.sender.district + ' - ' + result.sender.city,
                senderPhone: result.sender.phone,
                receiverName: result.receiver.name,
                receiverAddress: result.receiver.address + ' - ' + result.receiver.district + ' - ' + result.receiver.city,
                receiverPhone: result.receiver.phone,
                promotionCode: result.promotion_id,
                totalPrice: result.total,
                currentPosition: currentPos,
                shipperId: result.shipper_id,
                newOrderStatus: result.order_status.name,
                cod: result.cod,
                payer: result.payer,
                packageNote: result.package.note
            });

            var origin = result.sender.address+","+result.sender.district+",Đà Nẵng, Việt Nam";
            var destination = result.receiver.address+","+result.receiver.district+",Đà Nẵng, Việt Nam";

            //get shipper infor if any
            if (result.shipper_id != 0 && (result.order_status.id == 3 || result.order_status.id == 5 || result.order_status.id == 7 || result.order_status.id == 11 || result.order_status.id == 12)) {
                fetch(CONSTANTS.CDV_URL_SHIPPER_INFO+result.shipper_id, {
                    headers: {
                        'Authorization': this.state.userToken
                    }
                })
                .then((response) => response.json())
                .then((responseJson) => {

                    this.setState({
                        shipperName: responseJson.result.profile.full_name,
                        shipperPhone: responseJson.result.phone,
                        shipperToken: responseJson.result.token
                    });
                
                    //get distance value
                    fetch(CONSTANTS.GOOGLE_URL+'origin='+origin+'&destination='+destination+'&key='+CONSTANTS.GOOGLE_KEY+'&mode=json&language=vi')
                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({
                            distance: responseJson.routes[0].legs[0].distance.text,
                            isLoading: false
                        });
                    });
                })
            } else {
                fetch(CONSTANTS.GOOGLE_URL+'origin='+origin+'&destination='+destination+'&key='+CONSTANTS.GOOGLE_KEY+'&mode=json&language=vi')
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        distance: responseJson.routes[0].legs[0].distance.text,
                        isLoading: false
                    });
                });
            }
        });
    }

    showMeTheRouteClicked() {
        this.props.navigation.navigate('ShowMap', {
            orderId: this.state.orderId,
            orderCode: this.state.orderCode,
            userToken: this.state.userToken
        });
    }

    updateOrder() {
        this.props.navigation.navigate('UpdateOrderStatus', {
            orderId: this.state.orderId,
            orderCode: this.state.orderCode,
            userToken: this.state.userToken,
            orderStatus: this.state.orderStatus,
            shipperName: this.state.shipperName,
            updateOrderData: this.updateOrderData.bind(this)
        });
    }

    orderHistory() {
        this.props.navigation.navigate('OrderHistory', {
            orderCode: this.state.orderCode,
            userToken: this.state.userToken
        });
    }

    shipperInfoClicked() {
        Alert.alert('Cân Đẩu Vân - Express', 'Tính năng này đang được cập nhật, Xin vui lòng quay lại sau nhé.'+this.state.userRole);
    }

    chatWithShipperClicked() {
        Alert.alert('Cân Đẩu Vân - Express', 'Tính năng này đang được cập nhật, Xin vui lòng quay lại sau nhé.')
    }

    render() {

        var resultData = this.state.isLoading ?
        ( <ActivityIndicator
            style={styles.loading}
            size='large'/>
        ) :
        (
            <View style={{flex: 1}}>
                <ScrollView style={styles.itemContentContainer}>
                    <View style={styles.infoContainer}>
                        <View style={styles.transferContainer}>
                            <Text style={styles.orderStatusInformation}>TÌNH TRẠNG ĐƠN HÀNG</Text>
                            <Text style={styles.orderStatusInformationValue}>{this.state.orderStatus}</Text>
                        </View>
                        <View style={styles.stepIndicator}>
                            <StepIndicator
                            stepCount={3}
                            customStyles={thirdIndicatorStyles}
                            currentPosition={this.state.currentPosition}
                            labels={["TẠO ĐƠN","ĐANG CHUYỂN","HOÀN THÀNH"]}
                            />
                        </View>
                    </View>

                    <View style={styles.infoContainer}>
                        <Text style={styles.senderInformation}>ĐỊA CHỈ NHẬN HÀNG</Text>
                        <View style={styles.itemNameContainer}>
                            <Icon name="ios-contact-outline" size={30} color="#3498db" />
                            <Text style={styles.itemNameValue}>{this.state.senderName}</Text>
                        </View>
                        <View style={styles.itemAddressContainer}>
                            <Icon name="ios-pin-outline" size={30} color="#3498db" style={{paddingLeft: 3}} />
                            <Text numberOfLines={5} style={styles.itemAddressValue}>{this.state.senderAddress}</Text>
                        </View>
                        <View style={styles.itemPhoneContainer}>
                            <TouchableOpacity onPress={() => Communications.phonecall(this.state.senderPhone, true)}>
                                <View style={{flexDirection: 'row'}}>
                                    <Icon name="ios-call-outline" size={30} color="#3498db" style={{paddingLeft: 3}} />
                                    <Text style={styles.itemPhoneValue}>{this.state.senderPhone}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.infoContainer}>
                        <Text style={styles.senderInformation}>ĐỊA CHỈ GIAO HÀNG</Text>
                        <View style={styles.itemNameContainer}>
                            <Icon name="ios-contact-outline" size={30} color="#3498db" />
                            <Text style={styles.itemNameValue}>{this.state.receiverName}</Text>
                        </View>
                        <View style={styles.itemAddressContainer}>
                            <Icon name="ios-pin-outline" size={30} color="#3498db" style={{paddingLeft: 3}} />
                            <Text numberOfLines={5} style={styles.itemAddressValue}>{this.state.receiverAddress}</Text>
                        </View>
                        <View style={styles.itemPhoneContainer}>
                            <TouchableOpacity onPress={() => Communications.phonecall(this.state.receiverPhone, true)}>
                                <View style={{flexDirection: 'row'}}>
                                    <Icon name="ios-call-outline" size={30} color="#3498db" style={{paddingLeft: 3}} />
                                <Text style={styles.itemPhoneValue}>{this.state.receiverPhone}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.infoContainer}>
                        <Text style={styles.senderInformation}>TÓM TẮT ĐƠN HÀNG</Text>
                        <View style={styles.itemNameContainer}>
                            <Icon name="ios-speedometer-outline" size={30} color="#3498db" />
                            <Text style={styles.itemNameValue}>{this.state.orderType}</Text>
                        </View>
                        <View style={styles.itemAddressContainer}>
                            <Icon name="ios-pricetag-outline" size={30} color="#3498db" />
                            <Text style={styles.itemAddressValue}>{this.state.promotionCode}</Text>
                        </View>
                        <View style={styles.itemPhoneContainer}>
                            <Icon name="ios-cash-outline" size={30} color="#3498db" />
                            <Text style={styles.itemPhoneValue}>
                            PHÍ: {this.state.totalPrice.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} VND
                            </Text>
                        </View>
                        <View style={styles.itemPhoneContainer}>
                            <Icon name="ios-barcode-outline" size={30} color="#3498db" />
                            <Text style={styles.itemPhoneValue}>
                            COD: {this.state.cod.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} VND
                            </Text>
                        </View>
                        {(this.state.payer == "Sender") && (
                            <View style={styles.itemPhoneContainer}>
                                <Icon name="ios-alert-outline" size={30} color="#3498db" />
                                <Text style={styles.itemPhoneValue}>
                                    Free Ship
                                </Text>
                            </View>
                        )}
                        <View style={styles.itemPhoneContainer}>
                            <Icon name="ios-document-outline" size={30} color="#3498db" />
                            <Text style={styles.itemPhoneValue}>
                                {this.state.packageNote}
                            </Text>
                        </View>
                        <View style={styles.itemDistanceContainer}>
                            <Icon name="ios-swap-outline" size={30} color="#3498db" />
                            <Text style={styles.itemDistanceValue}>{this.state.distance}</Text>
                        </View>
                    </View>

                    {(this.state.shipperId != 0 && (this.state.orderStatusId == 3 || this.state.orderStatusId == 5 || this.state.orderStatusId == 7 || this.state.orderStatusId == 11 || this.state.orderStatusId == 12)) && (
                        <View style={styles.infoContainer}>
                            <Text style={styles.senderInformation}>NGƯỜI GIAO HÀNG</Text>
                            <View style={styles.itemNameContainer}>
                                <Icon name="ios-contact-outline" size={30} color="#3498db" />
                                <Text style={styles.itemNameValue}>{this.state.shipperName}</Text>
                            </View>
                            <View style={styles.itemPhoneContainer}>
                                <TouchableOpacity onPress={() => Communications.phonecall(this.state.shipperPhone, true)}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Icon name="ios-call-outline" size={30} color="#3498db" style={{paddingLeft: 3}} />
                                        <Text style={styles.itemPhoneValue}>{this.state.shipperPhone}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}

                </ScrollView>

                <ActionButton buttonColor="#1a699e" icon={upIcon} bgColor="#bdc3c7" bgOpacity={0.8}>
                    {(this.state.userRole != "Người Dùng") && (
                        <ActionButton.Item buttonColor='#8e44ad' title="Chỉ đường cho tôi" onPress={this.showMeTheRouteClicked.bind(this)}>
                            <Icon name="ios-map-outline" style={styles.actionButtonIcon} />
                        </ActionButton.Item>
                    )}
                    {(this.state.userRole != "Người Dùng") && (
                        <ActionButton.Item buttonColor='#d35400' title="Cập nhật Đơn Hàng" onPress={this.updateOrder.bind(this)}>
                        <Icon name="ios-clipboard-outline" style={styles.actionButtonIcon} />
                        </ActionButton.Item>
                    )}
                    {(this.state.userRole === "Người Dùng") && (
                        <ActionButton.Item buttonColor='#8e44ad' title="Thông Tin Người giao hàng" onPress={this.shipperInfoClicked.bind(this)}>
                        <Icon name="ios-bicycle-outline" style={styles.actionButtonIcon} />
                        </ActionButton.Item>
                    )}
                    {(this.state.userRole === "Người Dùng") && (
                        <ActionButton.Item buttonColor='#16a085' title="Chat với Người giao hàng" onPress={this.chatWithShipperClicked.bind(this)}>
                        <Icon name="ios-chatboxes-outline" style={styles.actionButtonIcon} />
                        </ActionButton.Item>
                    )}
                    <ActionButton.Item buttonColor='#2c3e50' title="Lịch sử Đơn Hàng" onPress={this.orderHistory.bind(this)}>
                        <Icon name="ios-archive-outline" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                </ActionButton>
            </View>
        );

        return (
            <View style={styles.container}>
                <StatusBar
                barStyle="light-content"
                />
                <View style={styles.topViewContainer}>
                <TouchableOpacity style={styles.topViewButtonLeft} onPress={this.goBack.bind(this)}>
                    <Icon name="ios-arrow-back-outline" size={30} color="white" style={styles.iconStyle} />
                </TouchableOpacity>
                <Text style={styles.topViewTitle}>Đơn Hàng #{this.state.orderCode}</Text>
                <TouchableOpacity style={styles.topViewButtonRight} onPress={this.goRefresh.bind(this)}>
                    <Icon name="ios-refresh-outline" size={30} color="white" style={styles.iconStyle} />
                </TouchableOpacity>
                </View>
                { resultData }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA'
    },
    topViewContainer: {
        height: height / 10,
        backgroundColor: '#1a699e',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    topViewButtonLeft: {
        paddingLeft: 10
    },
    topViewButtonRight: {
        paddingRight: 10
    },
    topViewTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: "700",
        // fontFamily: 'Roboto'
        ...Platform.select({
        android: {
            fontFamily: 'Roboto'
        }
        })
    },
    iconStyle: {
        width: 25,
        height: 25
    },
    itemContentContainer: {
        padding: 10
    },
    senderInformation: {
        padding: 10,
        fontSize: 13,
        color: '#2980b9',
        fontWeight: '900'
    },
    infoContainer: {
        backgroundColor: '#F5F5F5',
        shadowColor: '#4e4e4f',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        marginBottom: 10
    },
    itemNameContainer: {
        paddingLeft: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    itemNameValue: {
        marginLeft: 10
    },
    itemAddressContainer: {
        flexDirection: 'row',
        marginTop: 10,
        marginLeft: 20,
        alignItems: 'center'
    },
    itemAddressValue: {
        marginLeft: 12,
        flexDirection: 'row',
        flexWrap:'wrap',
        flex: 1
    },
    itemPhoneContainer: {
        flexDirection: 'row',
        marginTop: 10,
        marginLeft: 20,
        alignItems: 'center'
    },
    itemPhoneValue: {
        marginLeft: 10
    },
    itemStatusContainer: {
        flexDirection: 'row',
        marginTop: 10,
        marginLeft: 25,
        alignItems: 'center'
    },
    itemStatusValue: {
        marginLeft: 10
    },
    transferContainer: {
        alignItems: 'center'
    },
    orderStatusInformation: {
        paddingTop: 10,
        fontSize: 15,
        color: '#2980b9',
        fontWeight: '900'
    },
    orderStatusInformationValue: {
        marginTop: 5
    },
    transferStatusContainer: {
        padding: 10,
        marginTop: 5,
        flexDirection: 'row'
    },
    orderCreatedContainer: {
        alignItems: 'center',
        flex: 1
    },
    orderCreateValue: {
        fontSize: 14
    },
    orderCreateDate: {
        fontSize: 10
    },
    stepIndicator: {
        marginVertical:20,
    },
    actionButtonIcon: {
        fontSize: 25,
        height: 22,
        color: 'white',
    },
    itemDistanceContainer: {
        flexDirection: 'row',
        marginTop: 10,
        marginLeft: 25,
        alignItems: 'center'
    },
    itemDistanceValue: {
        padding: 5
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal: {
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1a699e'
    },
    modal3: {
        height: 200,
        width: 300
    },
    orderModalTitle: {
        paddingTop: 20,
        color: 'white',
        fontSize: 17,
        fontWeight: '800'
    },
    senderAddressInfomation: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10
    },
    confirmButton: {
        width: 110,
        height: 40,
        backgroundColor: '#2c3e50',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    },
    confirmButtonText: {
        color: '#ecf0f1',
        fontWeight: '700'
    },
    popup: {
        paddingLeft: 10,
        paddingBottom: 10,
        paddingRight: 10,
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        //width: 270,
        //height: 220
        width: (width - 50) - 150,
        height: (height / 2) - 150
    },
    popupFooter: {
        position: 'absolute',
        flex: 0.1,
        left: 10,
        right: 10,
        bottom: -2,
        alignItems: 'center'
    },
    popupText: {
        paddingTop: 10
    },
    boldText: {
        fontWeight: 'bold'
    },
    boldTextPrice: {
        fontWeight: 'bold',
        color: '#16a085'
    }
});