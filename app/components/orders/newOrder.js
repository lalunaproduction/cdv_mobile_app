import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity,Dimensions, Platform, Image, PropTypes, StatusBar,
ListView, ActivityIndicator, AsyncStorage, ScrollView, TextInput, Alert, BackHandler
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import { NavigationActions } from 'react-navigation';
import * as CONSTANTS from '../../config/constants'
import { Fumi } from 'react-native-textinput-effects';
import Picker from 'react-native-picker';
import Button from 'apsl-react-native-button';
import PopupDialog, { DialogTitle, SlideAnimation, DialogButton } from 'react-native-popup-dialog';

const drawerIcon = <Icon name="navicon" size={50} color="#000" />
const { width, height } = Dimensions.get('window');
const upIcon = <Icon name="ios-arrow-up-outline" size={30} color="#fff" />
const refreshIcon = <Icon name="ios-refresh-outline" size={30} color="#fff" />

export default class NewOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userToken: '',
            senderName: '',
            senderAddress: '',
            senderDistrict: '',
            senderPhone: '',
            receiverName: '',
            receiverAddress: '',
            receiverDistrict: '',
            receiverPhone: '',
            packageWidth: '',
            packageHeight: '',
            packageWeight: '',
            packageNote: '',
            shipType: '',
            promotionCode: '',
            cod: '',
            isLoading: true,
            tempDistance: '',
            tempPrice: '',
            popupTitle: '',
            popupType: '',
            errorMsg: '',
        }
    }

    goBack() {
        this.props.navigation.goBack(null);
    }

    goRefresh() {

    }

    _createDistrictData() { 
        let districts = ["Hải Châu","Thanh Khê","Cẩm Lệ","Sơn Trà","Ngũ Hành Sơn","Liên Chiểu","Hoà Vang"];
        return districts;
    }
    
    _createShipTypesData() { 
        let shipTypes = ["Chuyển Thường","Chuyển Nhanh","Chuyển Siêu Tốc"];
        return shipTypes;
    }

    _showSenderDistrictPicker() {
        Picker.init({
            pickerData: this._createDistrictData(),
            pickerToolBarFontSize: 18,
            pickerFontSize: 18,
            pickerFontColor: [41, 128, 185,1],
            pickerConfirmBtnText: "Xác Nhận",
            pickerCancelBtnText: "Huỷ",
            pickerTitleText: "Chọn Quận",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                this.setState({
                    senderDistrict: pickedValue[0].toString()
                });
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            }
        });
        Picker.show();
    }

    _showReceiverDistrictPicker() {
        Picker.init({
            pickerData: this._createDistrictData(),
            pickerToolBarFontSize: 18,
            pickerFontSize: 18,
            pickerFontColor: [41, 128, 185,1],
            pickerConfirmBtnText: "Xác Nhận",
            pickerCancelBtnText: "Huỷ",
            pickerTitleText: "Chọn Quận",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                this.setState({
                    receiverDistrict: pickedValue[0].toString()
                });
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            }
        });
        Picker.show();
    }

    _showShipTypePicker() {
        Picker.init({
            pickerData: this._createShipTypesData(),
            pickerToolBarFontSize: 18,
            pickerFontSize: 18,
            pickerFontColor: [41, 128, 185,1],
            pickerConfirmBtnText: "Xác Nhận",
            pickerCancelBtnText: "Huỷ",
            pickerTitleText: "Kiểu Giao Hàng",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                this.setState({
                    shipType: pickedValue[0].toString()
                });
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            }
        });
        Picker.show();
    }

    selectShipType() {
        this._showShipTypePicker();
    }

    selectSenderDistrict() {
        this._showSenderDistrictPicker();
    }

    selectReceiverDistrict() {
        this._showReceiverDistrictPicker();
    }

    tempCaculator() {
        if (this.state.senderAddress != "" &&
            this.state.receiverAddress != "" &&
            this.state.shipType != "" ) 
        {
            this.setState({
                isLoading: true
            });

            fetch(CONSTANTS.CDV_URL_ORDER_CAL, {
                method: 'POST',
                headers: {
                    'Authorization': this.state.userToken,
                    'Accept' : 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    sender_address: this.state.senderAddress,
                    receiver_address: this.state.receiverAddress,
                    ship_type: this.state.shipType,
                    promotion_code: this.state.promotionCode
                })
            })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.code == 200) {
                    var result = responseJson.result;

                    this.setState({
                        senderAddress: result.from.address,
                        senderDistrict: result.from.district,
                        receiverAddress: result.to.address,
                        receiverDistrict: result.to.district,
                        tempDistance: result.distanceText,
                        tempPrice: result.totalPrice,
                        isLoading: false,
                        popupType: "failed",
                        popupTitle: "TẠM TÍNH"
                    });

                    this.popupDialog.show(() => {
                        console.log('show temp caculating dialog');
                    });
                } else {
                    this.setState({
                        isLoading: false
                    });
                }
            });
        }
        else {
            Alert.alert('Cân Đẩu Vân - Express', 'Vui lòng nhập đầy đủ các thông tin cần thiết, để Tạm Tính', [
                {text: 'OK', onPress: () => console.log("ok")}
            ]);
        }
    }

    onCreateOrder() {
        if (this.state.senderName != "" &&
            this.state.senderAddress != "" &&
            this.state.senderPhone != "" &&
            this.state.receiverAddress != "" &&
            this.state.receiverName != "" &&
            this.state.receiverPhone != "" &&
            this.state.packageHeight != "" &&
            this.state.packageWeight != "" &&
            this.state.packageWidth != "" &&
            this.state.shipType != ""
        ) {
            this.setState({
                tempDistance: '',
                tempPrice: '',
                isLoading: true
            });

            fetch(CONSTANTS.CDV_URL_CREATE_ORDER, {
                method: 'POST',
                headers: {
                    'Authorization': this.state.userToken,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    sender_name: this.state.senderName,
                    sender_address: this.state.senderAddress,
                    sender_phone: this.state.senderPhone,
                    receiver_name: this.state.receiverName,
                    receiver_address: this.state.receiverAddress,
                    receiver_phone: this.state.receiverPhone,
                    ship_type: this.state.shipType,
                    package_height: this.state.packageHeight,
                    package_weight: this.state.packageWeight,
                    package_width: this.state.packageWidth,
                    package_note: this.state.packageNote,
                    cod: this.state.cod,
                    promotion_code: this.state.promotionCode
                })
            })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.code == 200) {
                    var result = responseJson.result;
                    this.setState({
                        tempDistance: responseJson.distance,
                        tempPrice: result.total,
                        popupType: "passed",
                        popupTitle: "TẠO ĐƠN HÀNG MỚI",
                        isLoading: false
                    });
                    this.popupDialog.show(() => {
                        console.log('show temp caculating dialog');
                    });
                } else {
                    this.setState({
                        popupType: "failed",
                        popupTitle: "TẠO ĐƠN HÀNG MỚI",
                        errorMsg: responseJson.messages,
                        isLoading: false
                    });
                    this.popupDialog.show(() => {
                        console.log('show temp caculating dialog');
                    });
                }
            });

        } else {
            Alert.alert('Cân Đẩu Vân - Express', 'Vui lòng nhập đầy đủ các thông tin cần thiết, để Tạo Đơn Hàng', [
                {text: 'OK', onPress: () => console.log("ok")}
            ]);
        }

    }

    dismissPopup() {
        this.popupDialog.dismiss();
    }

    continueOrder() {
        this.setState({
            receiverName: '',
            receiverAddress: '',
            receiverPhone: '',
            tempDistance: '',
            packageHeight: '',
            packageWeight: '',
            packageWidth: '',
            errorMsg: ''
        });
        this.popupDialog.dismiss();
    }

    backToListOrder() {
        this.props.navigation.goBack(null);
    }

    componentWillMount() {
        var that = this;
        BackHandler.addEventListener('hardwareBackPress', function() {
            that.goBack.bind(this);
        });
    }

    componentDidMount() {
        const { userToken } = this.props.navigation.state.params;
        this.setState({
            userToken: userToken
        });

        fetch(CONSTANTS.CDV_URL_GET_PROFILE, {
            headers: {
                'Authorization': userToken 
            }
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                senderName: responseJson.result.profile.full_name,
                senderAddress: responseJson.result.profile.address,
                senderDistrict: responseJson.result.profile.district,
                senderPhone: responseJson.result.phone,
                isLoading: false
            });
        });
    }

    render() {
        const { getToken } = this.props.navigation.state.params;

        var renderData = this.state.isLoading ?
        (<ActivityIndicator
            style={styles.loading}
            size='large'/>
        ) : (
            <ScrollView style={styles.itemContentContainer} ref="_scrollView">
                {/* THÔNG TIN NGƯỜI GỞI HÀNG */}
                <View style={styles.infoContainer}>
                    <Text style={styles.senderInformation}>NGƯỜI GỞI</Text>
                    <View style={styles.senderNameInfomation}>
                        <Fumi
                            label={'Tên Người Gởi'}
                            ref= {(el) => { this.senderName = el; }}
                            onChangeText={(senderName) => this.setState({senderName})}
                            value={this.state.senderName}
                            iconClass={Icon}
                            iconName={'ios-contact-outline'}
                            iconColor={'#16a085'}
                            height={50}
                        />
                    </View>
                    <View style={styles.senderAddressInfomation}>
                        <Fumi
                            label={'Địa Chỉ Lấy Hàng'}
                            ref= {(el) => { this.senderAddress = el; }}
                            onChangeText={(senderAddress) => this.setState({senderAddress})}
                            value={this.state.senderAddress +" ,"+ this.state.senderDistrict}
                            iconClass={Icon}
                            iconName={'ios-pin-outline'}
                            iconColor={'#16a085'}
                            height={50}
                        />
                    </View>
                    <View style={styles.senderAddressInfomation}>
                        <Fumi
                            label={'Số Điện Thoại'}
                            ref= {(el) => { this.senderPhone = el; }}
                            onChangeText={(senderPhone) => this.setState({senderPhone})}
                            value={this.state.senderPhone}
                            iconClass={Icon}
                            iconName={'ios-call-outline'}
                            iconColor={'#16a085'}
                            height={50}
                            keyboardType="numeric"
                        />
                    </View>
                </View>
                {/* KẾT THÚC THÔNG TIN NGƯỜI GỞI HÀNG */}

                {/* THÔNG TIN NGƯỜI NHẬN HÀNG */}
                <View style={styles.infoContainer}>
                    <Text style={styles.senderInformation}>NGƯỜI NHẬN</Text>
                    <View style={styles.senderNameInfomation}>
                        <Fumi
                            label={'Tên Người Nhận'}
                            ref= {(el) => { this.receiverName = el; }}
                            onChangeText={(receiverName) => this.setState({receiverName})}
                            value={this.state.receiverName}
                            iconClass={Icon}
                            iconName={'ios-contact-outline'}
                            iconColor={'#3498db'}
                            height={50}
                        />
                    </View>
                    <View style={styles.senderAddressInfomation}>
                        <Fumi
                            label={'Địa Chỉ Nhận Hàng'}
                            ref= {(el) => { this.receiverAddress = el; }}
                            onChangeText={(receiverAddress) => this.setState({receiverAddress})}
                            value={this.state.receiverAddress}
                            iconClass={Icon}
                            iconName={'ios-pin-outline'}
                            iconColor={'#3498db'}
                            height={50}
                        />
                    </View>
                    <View style={styles.senderAddressInfomation}>
                        <Fumi
                            label={'Số Điện Thoại'}
                            ref= {(el) => { this.receiverPhone = el; }}
                            onChangeText={(receiverPhone) => this.setState({receiverPhone})}
                            value={this.state.receiverPhone}
                            iconClass={Icon}
                            iconName={'ios-call-outline'}
                            iconColor={'#3498db'}
                            height={50}
                            keyboardType="numeric"
                        />
                    </View>
                </View>
                {/* KẾT THÚC THÔNG TIN NGƯỜI NHẬN HÀNG */}

                {/* THÔNG TIN MÓN HÀNG */}
                <View style={styles.infoContainer}>
                    <Text style={styles.senderInformation}>THÔNG TIN MÓN HÀNG</Text>
                    <View style={styles.senderNameInfomation}>
                        <Fumi
                            label={'Chiều Cao (cm)'}
                            ref= {(el) => { this.packageHeight = el; }}
                            onChangeText={(packageHeight) => this.setState({packageHeight})}
                            value={this.state.packageHeight}
                            iconClass={Icon}
                            iconName={'ios-resize-outline'}
                            iconColor={'#3498db'}
                            height={50}
                            keyboardType="numeric"
                        />
                    </View>
                    <View style={styles.senderAddressInfomation}>
                        <Fumi
                            label={'Chiều Rộng (cm)'}
                            ref= {(el) => { this.packageWidth = el; }}
                            onChangeText={(packageWidth) => this.setState({packageWidth})}
                            value={this.state.packageWidth}
                            iconClass={Icon}
                            iconName={'ios-resize-outline'}
                            iconColor={'#3498db'}
                            height={50}
                            keyboardType="numeric"
                        />
                    </View>
                    <View style={styles.senderAddressInfomation}>
                        <Fumi
                            label={'Cân Nặng (kg)'}
                            ref= {(el) => { this.packageWeight = el; }}
                            onChangeText={(packageWeight) => this.setState({packageWeight})}
                            value={this.state.packageWeight}
                            iconClass={Icon}
                            iconName={'ios-code-download-outline'}
                            iconColor={'#3498db'}
                            height={50}
                            keyboardType="numeric"
                        />
                    </View>
                    <View style={styles.senderAddressInfomation}>
                        <Fumi
                            label={'Ghi Chú'}
                            ref= {(el) => { this.packageNote = el; }}
                            onChangeText={(packageNote) => this.setState({packageNote})}
                            value={this.state.packageNote}
                            iconClass={Icon}
                            iconName={'ios-clipboard-outline'}
                            iconColor={'#3498db'}
                            height={50}
                        />
                    </View>
                </View>
                {/* KẾT THÚC THÔNG TIN MÓN HÀNG */}
                {/* THÔNG TIN CHUNG */}
                <View style={styles.infoContainer}>
                    <Text style={styles.senderInformation}>THÔNG TIN CHUNG</Text>
                    <View style={styles.senderNameInfomation}>
                        <TouchableOpacity onPress={this.selectShipType.bind(this)}>
                            <Fumi
                                label={'Chọn Kiểu Giao Hàng'}
                                ref= {(el) => { this.shipType = el; }}
                                onChangeText={(shipType) => this.setState({shipType})}
                                value={this.state.shipType}
                                iconClass={Icon}
                                editable={false}
                                iconName={'ios-flash-outline'}
                                iconColor={'#3498db'}
                                height={50}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.senderAddressInfomation}>
                        <Fumi
                            label={'COD'}
                            ref= {(el) => { this.cod = el; }}
                            onChangeText={(cod) => this.setState({cod})}
                            value={this.state.cod}
                            iconClass={Icon}
                            iconName={'ios-card-outline'}
                            iconColor={'#3498db'}
                            height={50}
                            keyboardType="numeric"
                        />
                    </View>
                    <View style={styles.senderAddressInfomation}>
                        <Fumi
                            label={'Mã Khuyến Mãi'}
                            ref= {(el) => { this.promotionCode = el; }}
                            onChangeText={(promotionCode) => this.setState({promotionCode})}
                            value={this.state.promotionCode}
                            iconClass={Icon}
                            iconName={'ios-card-outline'}
                            iconColor={'#3498db'}
                            height={50}
                        />
                    </View>
                </View>
                {/* KẾT THÚC THÔNG TIN CHUNG */}
                <View style={styles.caculateContainer}>
                    <View style={styles.buttonContainer}>
                         <Button
                            style={styles.buttonCaculateStyle}
                            isLoading={false}
                            textStyle={styles.buttonTextStyle}
                            onPress={this.tempCaculator.bind(this)}>
                            Tạm Tính
                        </Button> 
                    </View>
                    <View style={styles.buttonContainer}>
                        <Button
                            style={styles.buttonOrderStyle}
                            isLoading={false}
                            textStyle={styles.buttonTextStyle}
                            onPress={ this.onCreateOrder.bind(this) }>
                            Tạo Đơn Hàng
                        </Button> 
                    </View>
                </View>
            </ScrollView>
        )

        return( 
            <View style={styles.container}>
                <StatusBar
                barStyle="light-content"
                />
                {/* HEADER */}
                <View style={styles.topViewContainer}>
                <TouchableOpacity style={styles.topViewButtonLeft} onPress={this.goBack.bind(this)}>
                    <Icon name="ios-arrow-back-outline" size={30} color="white" style={styles.iconStyle} />
                </TouchableOpacity>
                <Text style={styles.topViewTitle}>Tạo Đơn Hàng Mới</Text>
                <TouchableOpacity style={styles.topViewButtonRight} onPress={this.goRefresh.bind(this)}>
                    <Icon name="ios-refresh-outline" size={30} color="white" style={styles.iconStyle} />
                </TouchableOpacity>
                </View>
                {/* KẾT THÚC HEADER */}
                {/* NỘI DUNG MÀN HÌNH */}
                { renderData }
                {/* NỘI DUNG MÀN HÌNH */}
                {/*POPUP Define*/}
                
                <PopupDialog
                    dialogTitle={<DialogTitle title={this.state.popupTitle} />}
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    dialogAnimation = { new SlideAnimation({ slideFrom: 'top' }) }
                    dismissOnTouchOutside = {false}
                    width = {width - 50}
                    height = { height * 0.75  }
                    actions={
                        <View>
                            {(this.state.popupType == "failed") && (
                                <View style={{
                                    flexDirection:'row',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                    <DialogButton text="OK" align="center" onPress={this.dismissPopup.bind(this)}/>    
                                </View>
                            )}
                            {(this.state.popupType == "passed") && (
                                <View style={{
                                    flexDirection:'row',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                    <DialogButton text="TẠO MỚI" onPress={this.continueOrder.bind(this)}/>
                                    <DialogButton text="DANH SÁCH" onPress={this.backToListOrder.bind(this)}/> 
                                </View>
                            )}
                        </View>
                    }
                >
                    <View style={styles.popup}>
                        <View style={styles.logoContainer}>
                            <Image
                                style={styles.logo}
                                source={require('../../images/logo_bang.png')}
                            />
                        </View>
                        {(this.state.popupType == "passed") && (
                            <View style={{paddingTop: 10}}>
                                <Text style={styles.popupText}>Điểm Nhận: <Text style={styles.boldText}>{this.state.senderAddress}, {this.state.senderDistrict}</Text> </Text>
                                <Text style={styles.popupText}>Điểm trả: <Text style={styles.boldText}>{this.state.receiverAddress}, {this.state.receiverDistrict}</Text></Text>
                                <Text style={styles.popupText}>Kiểu Vận Chuyển: <Text style={styles.boldText}>{this.state.shipType} </Text></Text>
                                <Text style={styles.popupText}>Mã Khuyến Mãi: <Text style={styles.boldText}>{this.state.promotionCode}</Text></Text>
                                <Text style={styles.popupText}>Khoảng Cách: <Text style={styles.boldText}>{this.state.tempDistance} </Text></Text>
                                <Text style={styles.popupText}>Phí Giao Hàng: <Text style={styles.boldTextPrice}>{this.state.tempPrice.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} VND</Text></Text>
                            </View>
                        )}
                        {(this.state.popupType == "failed") && (
                            <View style={{paddingTop: 10}}>
                                <Text style={styles.popupText}><Text style={styles.boldText}>{this.state.errorMsg}</Text> </Text>
                            </View>
                        )}
                        
                    </View>
                </PopupDialog>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA'
    },
    topViewContainer: {
        height: height / 10,
        backgroundColor: '#1a699e',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    topViewButtonLeft: {
        paddingLeft: 10
    },
    topViewButtonRight: {
        paddingRight: 10
    },
    topViewTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: "700",
        // fontFamily: 'Roboto'
        ...Platform.select({
        android: {
            fontFamily: 'Roboto'
        }
        })
    },
    iconStyle: {
        width: 25,
        height: 25
    },
    itemContentContainer: {
        padding: 10
    },
    infoContainer: {
        backgroundColor: '#F5F5F5',
        shadowColor: '#4e4e4f',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        marginBottom: 10
    },
    caculateContainer: {
        backgroundColor: '#F5F5F5',
        shadowColor: '#4e4e4f',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        marginBottom: 10,
        flexDirection: 'row'
    },
    senderInformation: {
        paddingLeft: 10,
        paddingTop: 10,
        paddingRight: 10,
        fontSize: 13,
        color: '#2980b9',
        fontWeight: '900'
    },
    senderNameInfomation: {
        padding: 10
    },
    senderAddressInfomation: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonCaculateStyle: {
        borderColor: '#16a085',
        backgroundColor: '#16a085',
        flex: 1
    },
    buttonOrderStyle: {
        borderColor: '#2980b9',
        backgroundColor: '#2980b9',
        flex: 1
    },
    buttonTextStyle: {
        color: 'white'
    },
    buttonContainer: {
        padding: 10,
        flex: 1
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#1a699e'
    },
    modal3: {
        height: 400,
        width: 300
    },
    orderCompeleteContainer: {
        paddingTop: 15
    },
    orderCompleteText: {
        color: 'white',
        fontSize: 18,
        fontWeight: '800'
    },
    orderCompleteSubText: {
        paddingTop: 10,
        color: 'white',
        fontSize: 12,
        fontWeight: '600'
    },
    createNewOrder: {
        padding: 10,
        backgroundColor: '#16a085',
    },
    backToListOrders: {
        padding: 10,
        backgroundColor: '#8e44ad',
    },
    orderButtonText: {
        color: 'white',
        fontWeight: '700'
    },
    popup: {
        paddingLeft: 10,
        paddingBottom: 10,
        paddingRight: 10,
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        //width: 270,
        //height: 220
        width: (width - 50) - 150,
        height: (height / 2) - 150
    },
    popupFooter: {
        position: 'absolute',
        flex: 0.1,
        left: 10,
        right: 10,
        bottom: -2,
        alignItems: 'center'
    },
    popupText: {
        paddingTop: 10
    },
    boldText: {
        fontWeight: 'bold'
    },
    boldTextPrice: {
        fontWeight: 'bold',
        color: '#16a085'
    }
});