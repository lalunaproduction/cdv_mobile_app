import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput,
Dimensions, Platform, StatusBar,Keyboard, BackHandler,
ListView, ScrollView, Alert, ActivityIndicator } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';
import icMenu from '../../images/ic_menu.png';
import icSearch from '../../images/ic_search.png';
import metrics from '../../config/metrics'
import Geocoder from 'react-native-geocoder';
import { Fumi } from 'react-native-textinput-effects';
import * as CONSTANTS from '../../config/constants';
import Picker from 'react-native-picker';

const { width, height } = Dimensions.get('window');

export default class UpdateOrderStatus extends Component {

    constructor(props) {
        super(props);

        this.state = {
            listShipper: [],
            orderId: '',
            orderCode: '',
            userToken: '',
            orderStatus: '',
            shipperName: '',
            shipperId: '',
            content: '',
            isLoading: false
        }

    }

    goBack() {
        this.props.navigation.goBack(null);
    }

    goRefresh() {
        
    }

    _createStatusNameData() { 
        let statusNames = ["Mới Tạo", "Xác Nhận", "Đang Lấy Hàng", "Đã Lấy Hàng", "Đang Giao Hàng", "Chờ Giao Lại", "Giao Hàng Thành Công", "Chờ Trả Hàng", "Hoàn Tất", "Huỷ"];
        return statusNames;
    }

    _showStatusNamePicker() {
        Picker.init({
            pickerData: this._createStatusNameData(),
            pickerToolBarFontSize: 18,
            pickerFontSize: 18,
            pickerFontColor: [41, 128, 185,1],
            pickerConfirmBtnText: "Xác Nhận",
            pickerCancelBtnText: "Huỷ",
            pickerTitleText: "Chọn Trạng Thái",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                this.setState({
                     orderStatus: pickedValue[0].toString()
                }, () => {
                    //this.orderStatusDialog.show();
                });
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            }
        });
        Picker.show();
    }

    _showShipperPicker() {
        Picker.init({
            pickerData: this.state.listShipper,
            pickerToolBarFontSize: 18,
            pickerFontSize: 18,
            pickerFontColor: [41, 128, 185,1],
            pickerConfirmBtnText: "Xác Nhận",
            pickerCancelBtnText: "Huỷ",
            pickerTitleText: "Chọn Nhân Viên",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                this.setState({
                     shipperName: pickedValue[0].toString()
                });
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            }
        });
        Picker.show();
    }

    selectStatusName() {
        this._showStatusNamePicker();
    }

    selectShipper() {
        this._showShipperPicker();
    }

    getStatusIdByName(name) {
        switch(name) {
            case "Mới Tạo":
                return 1;
            case "Xác Nhận":
                return 2;
            case "Đang Lấy Hàng":
                return 3;
            case "Đã Lấy Hàng":
                return 4;
            case "Đang Giao Hàng":
                return 5;
            case "Chờ Giao Lại":
                return 6;
            case "Giao Hàng Thành Công":
                return 7;
            case "Chờ Trả Hàng":
                return 8;
            case "Đang Trả Hàng":
                return 9;
            case "Hoàn Tất":
                return 11;
            case "Huỷ":
                return 12;
            default:
                return 0;
        }
    }

    updateOrder() {
        Keyboard.dismiss();

        this.setState({
            isLoading: true
        });

        var statusId = this.getStatusIdByName(this.state.orderStatus);
        console.log("status ID is: "+statusId);

        if (this.state.shipperName != '') {
            fetch(CONSTANTS.CDV_URL_GET_SHIPPER_BY_FNAME, {
                method: 'POST',
                headers: {
                    'Authorization': this.state.userToken,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    full_name: this.state.shipperName
                })
            })
            .then((response) => response.json())
            .then((responseJson) => {
                var res = responseJson.result;

                this.setState({
                    shipperId: res.id
                }, () => {
                    fetch(CONSTANTS.CDV_URL_UPDATE_ORDER_STATUS+this.state.orderId, {
                        method: 'POST',
                        headers: {
                            'Authorization': this.state.userToken,
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            order_status_id: statusId,
                            shipper_id: this.state.shipperId,
                            content: this.state.content
                        })
                    })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        if(responseJson.status === "OK") {
                            this.props.navigation.state.params.updateOrderData(this.state.orderCode);
                            this.props.navigation.goBack(null);
                        }
                    });
                })
            });
        } else {
            fetch(CONSTANTS.CDV_URL_UPDATE_ORDER_STATUS+this.state.orderId, {
                method: 'POST',
                headers: {
                    'Authorization': this.state.userToken,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    order_status_id: statusId,
                    shipper_id: this.state.shipperId,
                    content: this.state.content
                })
            })
            .then((response) => response.json())
            .then((responseJson) => {
                if(responseJson.status === "OK") {
                    this.props.navigation.state.params.updateOrderData(this.state.orderCode);
                    this.props.navigation.goBack(null);
                }
            });
        }
    }

    componentWillMount() {
        var that = this;
        BackHandler.addEventListener('hardwareBackPress', function() {
            that.goBack.bind(this);
        });
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;
        this.setState({ 
            orderId: params.orderId,
            orderCode: params.orderCode, 
            userToken: params.userToken,
            orderStatus: params.orderStatus,
            shipperName: params.shipperName,
            isLoading: true
        });

        //getListShipper
        fetch(CONSTANTS.CDV_URL_GET_LIST_SHIPPER, {
            method: 'POST',
            headers: {
                'Authorization': this.state.userToken,
                'Content-Type': 'application/json'
            }
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if (responseJson.code == 200) {
                var lsItem = [];
                responseJson.result.forEach(function(item, index) {
                    lsItem.push(item.profile.full_name);
                });

                this.setState({
                    listShipper: lsItem,
                    isLoading: false
                });
            }
        });
    }

    render() {
        var renderData = this.state.isLoading ? 
        (<ActivityIndicator
            style={styles.loading}
            size='large'/>
        ) : 
        (
            <View style={{flex: 1}}>
                <View style={styles.orderStatusContainer}>
                    <Text style={styles.orderStatusText}>Tình Trạng ĐH: </Text>
                    <TouchableOpacity style={{width: width/2}} onPress={this.selectStatusName.bind(this)}>
                        <TextInput
                            style = {styles.orderInfoText}
                            ref={(el) => {this.orderStatus = el;}}
                            onChangeText={(orderStatus) => this.setState({orderStatus})}
                            value = {this.state.orderStatus}
                            height={40}
                            autoFocus={false}
                            editable={false}
                        />
                    </TouchableOpacity>
                </View>
                {(this.state.orderStatus == "Đang Lấy Hàng" || this.state.orderStatus == "Đang Giao Hàng") && (
                    <View style={styles.shipperContainer}>
                        <Text style={styles.orderStatusText}>NV giao hàng: </Text>
                        <TouchableOpacity style={{width: width/2}} onPress={this.selectShipper.bind(this)}>
                            <TextInput
                                style = {styles.orderInfoText}
                                ref={(el) => {this.shipperName = el;}}
                                onChangeText={(shipperName) => this.setState({shipperName})}
                                value = {this.state.shipperName}
                                height={40}
                                autoFocus={false}
                                editable={false}
                            />
                        </TouchableOpacity>
                    </View>
                )}
                <View style={styles.contentContainer}>
                    <Text style={styles.orderStatusText}>Nội dung: </Text>
                    <TextInput 
                        editable = {true}
                        maxLength = {200}
                        multiline = {true}
                        numberOfLines = {5}
                        ref= {(el) => { this.content = el; }}
                        onChangeText={(content) => this.setState({content})}
                        value={this.state.content}
                    />
                </View>
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <TouchableOpacity onPress={this.updateOrder.bind(this)}>
                        <Text> CẬP NHẬT </Text>
                    </TouchableOpacity>
                </View>
            </View> 
        )

        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="light-content"
                />
                <View style={styles.topViewContainer}>
                <TouchableOpacity style={styles.topViewButtonLeft} onPress={this.goBack.bind(this)}>
                    <Icon name="ios-arrow-back-outline" size={30} color="white" style={styles.iconStyle} />
                </TouchableOpacity>
                <Text style={styles.topViewTitle}>Đơn Hàng #{this.state.orderCode}</Text>
                <TouchableOpacity style={styles.topViewButtonRight} onPress={this.goRefresh.bind(this)}>
                    <Icon name="ios-refresh-outline" size={30} color="white" style={styles.iconStyle} />
                </TouchableOpacity>
                </View>
                {renderData}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA'
    },
    orderStatusContainer: {
        padding: 10,
        flexDirection: 'row',
    },
    shipperContainer: {
        padding: 10,
        flexDirection: 'row'
    },
    contentContainer: {
        padding: 10
    },
    topViewContainer: {
        height: height / 10,
        backgroundColor: '#1a699e',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    orderStatusText: {
        fontSize: 18,
        paddingTop: 8,
        color: '#2980b9'
    },
    orderInfoText: {
        fontSize: 16,
        color: '#2c3e50'
    },
    topViewButtonLeft: {
        paddingLeft: 10
    },
    topViewButtonRight: {
        paddingRight: 10
    },
    topViewTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: "700",
        // fontFamily: 'Roboto'
        ...Platform.select({
        android: {
            fontFamily: 'Roboto'
        }
        })
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

});