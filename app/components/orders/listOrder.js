import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity,
  Dimensions, Platform, Image, PropTypes, StatusBar,
  ListView, ActivityIndicator, AsyncStorage, FlatList
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';

import metrics from '../../config/metrics'
import * as CONSTANTS from '../../config/constants'
import imgProfile from '../../images/profile.jpg';
import icMenu from '../../images/ic_menu.png';
import icSearch from '../../images/ic_search.png';
import { MenuContext, Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger, } from 'react-native-popup-menu';
import { connect } from 'react-redux';

const drawerIcon = <Icon name="navicon" size={50} color="#000" />
const { width, height } = Dimensions.get('window');
const API_LIST_ORDER = CONSTANTS.CDV_URL_LIST_ORDER;
const API_CREATE_TOKEN = CONSTANTS.CDV_URL_CREATE_TOKEN;

class ListOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listData: [],
            isLoading: true,
            skip: 0,
            take: 15,
            userToken: '',
            orderStatus: ''
        }
    }

    getDateTimeString(dt) {
        var d = new Date(dt);
        var res = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
        return (<Text style={styles.itemOrderDate}>{res}</Text>);
    }

    onRefresh() {
        this.setState({
            skip: 0,
            take: 15,
            isLoading: true,
            listData: []
        }, () => {
            fetch(API_LIST_ORDER+"?skip="+this.state.skip+"&take="+this.state.take+"&orderStatus="+this.state.orderStatus, {
                headers: {
                    'Authorization': this.state.userToken 
                }
            })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listData: responseJson.result,
                    isLoading: false
                });
            });
        });
    }

    getReadyOrders() {
        this.setState({
            skip: 0,
            take: 15,
            isLoading: true,
            listData: [],
            orderStatus: 'Nok'
        }, () => {
            fetch(API_LIST_ORDER+"?skip="+this.state.skip+"&take="+this.state.take+"&orderStatus="+this.state.orderStatus, {
                headers: {
                    'Authorization': this.state.userToken 
                }
            })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listData: responseJson.result,
                    isLoading: false
                });
            });
        });
    }

    getCompleteOrder() {
        this.setState({
            skip: 0,
            take: 15,
            isLoading: true,
            listData: [],
            orderStatus: 'ok'
        }, () => {
            fetch(API_LIST_ORDER+"?skip="+this.state.skip+"&take="+this.state.take+"&orderStatus="+this.state.orderStatus, {
                headers: {
                    'Authorization': this.state.userToken 
                }
            })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listData: responseJson.result,
                    isLoading: false
                });
            });
        });
    }

    getAllOrders() {
        this.setState({
            skip: 0,
            take: 15,
            isLoading: true,
            listData: [],
            orderStatus: ''
        }, () => {
            fetch(API_LIST_ORDER+"?skip="+this.state.skip+"&take="+this.state.take, {
                headers: {
                    'Authorization': this.state.userToken 
                }
            })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listData: responseJson.result,
                    isLoading: false
                });
            });
        });
    }

    openDrawer() {
        this.props.navigation.navigate('DrawerOpen');
    }

    goToOrderDetail(orderCode) {
        this.props.navigation.navigate('OrderDetail', { code: orderCode, userToken: this.state.userToken });
    }

    componentWillMount() {

        this.props.dispatch({ type: 'GET_LOCATION' });

        AsyncStorage.getItem("USERTOKEN").then((value)=>{
            this.setState({
                userToken: value,
                orderStatus: 'Nok'
            });

            console.log(value);
            console.log(CONSTANTS.CDV_URL_LIST_ORDER+"?skip="+this.state.skip+"&take="+this.state.take);

            fetch(CONSTANTS.CDV_URL_LIST_ORDER+"?skip="+this.state.skip+"&take="+this.state.take+"&orderStatus=Nok", {
                headers: {
                    'Authorization': value
                }
            })
            .then((response) =>{
                console.log("abc"); 
                setTimeout(() => null, 0);  // workaround for issue-6679
                return response.json();
            })
            .then((responseJson) => {
                console.log("zozozo");
                console.log(responseJson.result);

                
                this.setState({
                    listData: responseJson.result,
                    isLoading: false
                });
            });

        });
    }

    componentDidMount() {
        // AsyncStorage.getItem('USERTOKEN').then((result) => {
        //     this.setState({
        //         userToken: result
        //     });

        //     fetch(CONSTANTS.CDV_URL_LIST_ORDER+"?skip="+this.state.skip+"&take="+this.state.take, {
        //         headers: {
        //             'Authorization': result 
        //         }
        //     })
        //     .then((response) => response.json())
        //     .then((responseJson) => {
        //         this.setState({
        //             listData: responseJson.result,
        //             isLoading: false
        //         });
        //     });
        // });
    }

    loadMore() {
        if (this.state.listData.length >= 15) {
            var orderURL = "";

            if (this.state.orderStatus === 'ok') {
                orderURL = CONSTANTS.CDV_URL_LIST_ORDER+"?skip="+this.state.skip+"&take="+this.state.take+"&orderStatus=ok";
            } else if (this.state.orderStatus === 'Nok') {
                orderURL = CONSTANTS.CDV_URL_LIST_ORDER+"?skip="+this.state.skip+"&take="+this.state.take+"&orderStatus=Nok";
            } else {
                orderURL = CONSTANTS.CDV_URL_LIST_ORDER+"?skip="+this.state.skip+"&take="+this.state.take;
            }

            this.setState({
                skip: this.state.skip + 15
            }, ()=>{
                fetch(orderURL, {
                    headers: {
                        'Authorization': this.state.userToken 
                    }
                })
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        listData: this.state.listData.concat(responseJson.result)
                    });
                    
                });
            })
        }
    }

    addNewOrder() {
        this.props.navigation.navigate('NewOrder', {
            userToken: this.state.userToken
        });
    }

    render() {
        var spinner = this.state.isLoading ? 
        (<ActivityIndicator style={styles.loading} size='large'/> ) : (
            <View style={{flex: 1}}>
                <FlatList
                    onEndReachedThreshold="0.2"
                    onEndReached={() => {this.loadMore()}}
                    data={this.state.listData}
                    keyExtractor={(item, index) => "#CDV-"+item.order_code}
                    renderItem={({item}) => 
                        <TouchableOpacity onPress={() => this.goToOrderDetail(item.order_code)}>
                            <View style={styles.mainItemsContainer}>
                                <View style={styles.itemCodeContainer}>
                                <Text style={styles.itemCodeContent}>#{item.order_code} ({item.sender.name})</Text>
                                <Text style={styles.itemOrderTotal}>
                                       
                                </Text>
                                {(item.order_status.name == "Giao Hàng Thành Công") && (
                                    <View style={styles.itemStatusSuccessContainer}>
                                        <Text style={styles.itemStatusContent}>{item.order_status.name}</Text>
                                    </View>
                                )}
                                {(item.order_status.name != "Giao Hàng Thành Công") && (
                                    <View style={styles.itemStatusContainer}>
                                        <Text style={styles.itemStatusContent}>{item.order_status.name}</Text>
                                    </View>
                                )}
                                </View>
                                <View style={styles.itemContentContainer}>
                                <View style={styles.leftContentContainer}>
                                    <Image style={styles.userProfile} source={imgProfile} />
                                    {this.getDateTimeString(item.created_at)}
                                    <Text style={styles.itemOrderTotal}>
                                        {item.total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")} VND
                                    </Text>
                                </View>
                                <View style={styles.rightContentContainer}>
                                    <View style={styles.itemSenderNameContainer}>
                                        <Icon name="ios-contact-outline" size={30} color="#3498db" />
                                        <Text style={styles.itemSenderNameValue}>{item.receiver.name}</Text>
                                    </View>
                                    <View style={styles.itemSenderAddressContainer}>
                                        <Icon name="ios-pin-outline" size={30} color="#3498db" style={{paddingLeft: 3}} />
                                        <Text numberOfLines={5} style={styles.itemSenderAddressValue}>{item.receiver.address} - {item.receiver.district} - Thành Phố {item.receiver.city}</Text>
                                    </View>
                                    <View style={styles.itemSenderPhoneContainer}>
                                        <Icon name="ios-call-outline" size={30} color="#3498db" style={{paddingLeft: 3}} />
                                        <Text style={styles.itemSenderPhoneValue}>{item.receiver.phone}</Text>
                                    </View>
                                    {(item.payer == "Sender") && (
                                        <View style={styles.itemSenderPhoneContainer}>
                                            <Icon name="ios-cash-outline" size={30} color="#3498db" style={{paddingLeft: 3}} />
                                            <Text style={styles.itemSenderPhoneValue}>Free Ship</Text>
                                        </View>
                                    )}
                                    
                                </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    }
                />
                <ActionButton
                    buttonColor="#2980b9"
                    bgColor="#bdc3c7" bgOpacity={0.8}
                >
                    <ActionButton.Item buttonColor='#16a085' title="Tạo Đơn Hàng Mới" onPress={this.addNewOrder.bind(this)}>
                    <Icon name="ios-create-outline" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                </ActionButton>
            </View>
        );

        return (
            <MenuContext>
                <View style={styles.container}>
                    <StatusBar barStyle="light-content" />
                    <View style={styles.topViewContainer}>
                        <TouchableOpacity style={styles.topViewButtonLeft} onPress={this.openDrawer.bind(this)}>
                            <Image style={styles.iconStyle} source={icMenu} />
                        </TouchableOpacity>
                        <Text style={styles.topViewTitle}>Đơn Hàng Của Tôi</Text>
                        <View style={{flexDirection: 'row'}}>
                            
                            {/* <TouchableOpacity style={styles.topViewButtonRight}>
                            <Image style={styles.iconStyle} source={icSearch} />
                            </TouchableOpacity> */}
                            <Menu style={styles.topViewButtonRight}>
                                <MenuTrigger customStyles={triggerStyles} text='&#8942;' />
                                <MenuOptions>
                                    <MenuOption customStyles={optionStyles} onSelect={this.getReadyOrders.bind(this)}>
                                        <View style={{flexDirection: 'row'}}>
                                            <Icon name="ios-alarm-outline" size={25} color="#c0392b" style={{paddingLeft: 3, paddingTop: 5}} />
                                            <Text style={styles.menuOptionItem}> Đơn Cần Giao</Text>
                                        </View>
                                    </MenuOption>    
                                    <MenuOption customStyles={optionStyles} onSelect={this.getCompleteOrder.bind(this)} >
                                        <View style={{flexDirection: 'row'}}>
                                            <Icon name="ios-checkmark-circle-outline" size={25} color="#16a085" style={{paddingLeft: 3, paddingTop: 5}} />
                                            <Text style={styles.menuOptionItem}>Đơn Giao Xong</Text>
                                        </View>
                                    </MenuOption>
                                    <MenuOption customStyles={optionStyles} onSelect={this.getAllOrders.bind(this)} >
                                        <View style={{flexDirection: 'row'}}>
                                            <Icon name="ios-clipboard-outline" size={25} color="#3498db" style={{paddingLeft: 3, paddingTop: 5}} />
                                            <Text style={styles.menuOptionItem}>Tất cả Đơn</Text>
                                        </View>
                                    </MenuOption>
                                </MenuOptions>
                            </Menu>
                            <TouchableOpacity style={styles.topViewButtonRight} onPress={this.onRefresh.bind(this)}>
                                <Icon name="ios-refresh-outline" size={30} color="white" style={styles.iconStyle} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {spinner}
                </View>
            </MenuContext>
        );
    }
}

export default connect()(ListOrder);

const triggerStyles = {
  triggerText: {
    color: 'white',
    fontSize: 25,
    width: 25,
    fontWeight: 'bold',
    paddingLeft: 5
  }
};

const optionStyles = {
    optionWrapper: {
        height: 45
    },
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA'
    },
    topViewContainer: {
        height: height / 10,
        backgroundColor: '#1a699e',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    topViewButtonLeft: {
        paddingLeft: 10
    },
    topViewButtonRight: {
        paddingRight: 10
    },
    topViewTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: "700",
        // fontFamily: 'Roboto'
        ...Platform.select({
        android: {
            fontFamily: 'Roboto'
        }
        })
    },
    iconStyle: {
        paddingTop: 1,
        width: 25,
        height: 25
    },
    mainItemsContainer: {
        backgroundColor: '#F5F5F5',
        marginLeft: 10,
        marginTop: 10,
        marginRight: 10,
        shadowColor: '#4e4e4f',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2
    },
    itemCodeContainer: {
        padding: 10,
        flexDirection: 'row'
    },
    itemCodeContent: {
        fontWeight: "700",
        fontSize: 16,
        color: '#2980b9'
    },
    itemStatusContainer: {
        marginLeft: 10,
        padding: 4,
        backgroundColor: '#2980b9'
    },
    itemStatusSuccessContainer: {
        marginLeft: 10,
        padding: 4,
        backgroundColor: '#16a085'
    },
    itemStatusContent: {
        fontSize: 10,
        color: 'white'
    },
    itemContentContainer: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
        flexDirection: 'row',

    },
    leftContentContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    rightContentContainer: {
        flex: 3
    },
    itemSenderNameContainer: {
        flexDirection: 'row',
        marginLeft: 10,
        marginTop: 5
    },
    itemSenderNameValue: {
        marginLeft: 10,
        marginTop: 6
    },
    itemSenderAddressContainer: {
        flexDirection: 'row',
        marginTop: 7,
        marginLeft: 7
    },
    itemSenderAddressValue: {
        marginLeft: 15,
        flexDirection: 'row',
        flexWrap:'wrap',
        flex: 1
    },
    itemSenderPhoneContainer: {
        flexDirection: 'row',
        marginTop: 7,
        marginLeft: 7
    },
    itemSenderPhoneValue: {
        marginLeft: 11,
        marginTop: 6
    },
    userProfile: {
        width: 70,
        height: 70,
        borderRadius: 35
    },
    itemOrderDate: {
        paddingTop: 5,
        fontSize: 12
    },
    itemOrderTotal: {
        paddingTop: 5,
        color: '#009688'
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
    menuIcon: {
        color: 'white',
        fontSize: 20
    },
    menuOptionItem: {
        padding: 7,
        fontSize: 15,
        fontWeight: 'bold'
    }
});

