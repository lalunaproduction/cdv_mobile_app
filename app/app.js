import React, { Component } from 'react';
import { View, Platform, PermissionsAndroid } from 'react-native';
import { Navigator } from './config/router';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

const defaultState = { myPosition: null };
const GEOLOCATION_OPTIONS = { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 };

const defaultProps = {
  enableHack: false,
  geolocationOptions: GEOLOCATION_OPTIONS,
};

const reducer = (state = defaultState, action) => {
    if (action.type === 'GET_LOCATION') {
        console.log("===bat dau zo get location===")
        if (Platform.OS === 'android') {
        PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
            .then(granted => {
                if (granted && this.mounted){
                    this.watchID = navigator.geolocation.watchPosition((position) => {
                        const currentPosition = JSON.stringify(position);
                        return { myPosition: currentPosition }
                    }, (error) => {
                        return { myPosition: null }
                    }, { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }); 
                }
            });
        } else {
            this.watchID = navigator.geolocation.watchPosition((position) => {
                const currentPosition = JSON.stringify(position);
                return { myPosition: currentPosition }
            }, (error) => {
                return { myPosition: null }
            }, { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }); 
        }
    }

    return state;
}

const store = createStore(reducer);

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Navigator />
            </Provider>
        );
    }
}